void write_tabledatalist(TABLEDATALIST ** tabledatalist, const char* table_name)
{
	
	/*printing of the content of the tabledatalist*/
	TABLEDATALIST 		* temp1,
						* temp2;
						
	int 				count = 0;
	
	char command[1000];
	command[0]='\0';
	strcpy(command,table_name);
	strcat(command,".txt");
	ofstream myfile (command);
	if (tabledatalist == NULL || tabledatalist[0] == NULL)
	{
		cout << "TABLE EMPTY";
		
	}
	temp2 = temp1 = tabledatalist[2];
	cout << "\n";
	do{
		if(temp1->up != tabledatalist[0])
		{
			do
			{
				if(temp1->data == NULL)
				{
					cout << "NULL \t";
					 myfile << "NULL \t";
				}
				else if(temp1->type.compare("VARCHAR")==0 || temp1->type.compare("CHAR")==0)
				{
					cout << *(static_cast<string *>(temp1->data)) << "\t";
					myfile << *(static_cast<string *>(temp1->data)) << "\t";
				}
				else if(temp1->type.compare("INT")==0)
				{
					cout << *(static_cast<int *>(temp1->data)) << "\t";
					myfile << *(static_cast<int *>(temp1->data)) << "\t";
				}
				else if(temp1->type.compare("BOOLEAN")==0)
				{
					cout << *(static_cast<bool *>(temp1->data)) << "\t";
					myfile << *(static_cast<bool *>(temp1->data)) << "\t";
				}
				else if(temp1->type.compare("FLOAT")==0)
				{
					printf("%2f \t", *(static_cast<float *>(temp1->data)));
					myfile << setprecision(2) << *(static_cast<float *>(temp1->data)) << "\t";
					
				}
				temp1 = temp1->next;	
			}while(temp1!=temp2);
			cout << "\n";	
		}
		temp2 = temp2->down;
		temp1 = temp2;		
	//}while(temp2 != NULL);
	}while(temp2 != tabledatalist[0]);
	myfile.close();
	count = row_count(temp2);
	if(count > 1)
		cout << "\n" << count << " rows in set\n";
	else
	{
		cout << "\n" << count << " row in set\n";
	}
	
	
}
