TABLEDATALIST ** delete_op(DELETEQUERYPARSE query)
{
	
	TABLEDATALIST 	** table = get_table(query.tablename), 
					** rows = NULL;
					
	if(table == NULL)
		return false;
	else
	{
		if(query.search_condition == NULL)
			return false;
		else
		{
			rows = condlist_selector(table,query.search_condition);
			
			if(delete_physical_operator(table, rows))
				return table;
			else
				return NULL;
		}
	}

}

bool delete_physical_operator(TABLEDATALIST ** tabledatalist, TABLEDATALIST ** rows)
{

	TABLEDATALIST ** 	tabledataTemp1 = rows,
						* tabledataTemp2 = tabledatalist[0], 
						* firstRow = NULL,
						* temp = NULL;
	
	string 				column_name, 
						stringTemp1;
						
	int 				i=0;
	
	if(tabledatalist == NULL || rows == NULL)
	{
		return false;
	}
	
	for(int i = 0; *(rows + i) != NULL; i++)
	{
		temp = *(rows + i);
		
		do
		{
			temp->up->down = temp->down;
			temp->down->up = temp->up;
			temp = temp->next;
			free(temp->prev);
		}while(temp != *(rows + i));
		
		free(temp);
	}
	
	
	return true;
}