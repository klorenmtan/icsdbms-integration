void swap_and_pattern(TABLEDATALIST ** table, CONDLIST * lolop, CONDLIST * search_condition, CONDLIST ** condlist_pool)
{
	CONDLIST 	** temp = condlist_pool;
	
	int 		count = 0;
	
	
	while(*(temp + count) !=  NULL)
	{
		count++;
		
	}
	
	if(lolop->op.compare("AND") == 0)
		if(cond_stat(table, lolop->prev) >  cond_stat(table, lolop->next))
		{
			switch_cond(lolop->prev, lolop->next);
			*(condlist_pool + count++) = search_duplicate(search_condition, condlist_pool);
		}
}

void swap_or_pattern(TABLEDATALIST ** table, CONDLIST * lolop, CONDLIST * search_condition, CONDLIST ** condlist_pool)
{
	CONDLIST	** temp = condlist_pool;
	
	int 		count = 0;
	
	while(*(temp + count) !=  NULL)
	{
		count++;
	}
	if(lolop->op.compare("OR") == 0)
		if(cond_stat(table, lolop->prev) <  cond_stat(table, lolop->prev))
		{
			switch_cond(lolop->prev, lolop->next);
			*(condlist_pool + count++) = search_duplicate(search_condition, condlist_pool);
		}
}

void elim_or_and_pattern(TABLEDATALIST ** table, CONDLIST * lolop, CONDLIST * search_condition, CONDLIST ** condlist_pool)
{
	
	CONDLIST 	** temp = condlist_pool;
	
	int count = 0;
	
	while(*(temp + count) !=  NULL)
	{
		count++;
	}
	
	if(lolop->prev->column_name.compare(lolop->next->column_name) == 0 )
	{
		// > OR > , > AND > 
		if(lolop->prev->op.compare(">") == 0 && lolop->next->op.compare(">") == 0) 
		{
			if(lolop->prev->type.compare("INT") == 0)
			{
				if(get_int(lolop->prev->value) < get_int(lolop->next->value))
				{
					if(lolop->op.compare("OR") == 0)
						elim_cond(lolop, "NEXT");
					else if(lolop->op.compare("AND") == 0)
					{
						elim_cond(lolop, "PREV");
					}
					
					*(condlist_pool + count++) = search_duplicate(search_condition, condlist_pool);
				}
				else if(get_int(lolop->prev->value) > get_int(lolop->next->value))
				{
					if(lolop->op.compare("OR") == 0)
						elim_cond(lolop, "PREV");
					else if(lolop->op.compare("AND") == 0)
						elim_cond(lolop, "NEXT");
					*(condlist_pool + count++) = search_duplicate(search_condition, condlist_pool);
				}
			}
		}
		// > OR = , > AND =
		else if(lolop->prev->op.compare(">") == 0 && lolop->next->op.compare("=") == 0)
		{
			if(lolop->prev->type.compare("INT") == 0)
			{
				if(get_int(lolop->prev->value) < get_int(lolop->next->value))
				{
					if(lolop->op.compare("OR") == 0)
						elim_cond(lolop, "NEXT");
					else if(lolop->op.compare("AND") == 0)
						elim_cond(lolop, "PREV");
						
					*(condlist_pool + count++) = search_duplicate(search_condition, condlist_pool);
				}
			}
		}
		// = OR > , = AND >
		else if(lolop->prev->op.compare("=") == 0 && lolop->next->op.compare(">") == 0)
		{
			if(lolop->prev->type.compare("INT") == 0)
			{
				if(get_int(lolop->prev->value) > get_int(lolop->next->value))
				{
					if(lolop->op.compare("OR") == 0)
						elim_cond(lolop, "PREV");
					else if(lolop->op.compare("AND") == 0)
						elim_cond(lolop, "NEXT");
					
					*(condlist_pool + count++) = search_duplicate(search_condition, condlist_pool);
				}
			}
		}
		//< OR < , < AND <
		else if(lolop->prev->op.compare("<") == 0 && lolop->next->op.compare("<") == 0) 
		{
			if(lolop->prev->type.compare("INT") == 0)
			{
				if(get_int(lolop->prev->value) > get_int(lolop->next->value))
				{
					if(lolop->op.compare("OR") == 0)
						elim_cond(lolop, "NEXT");
					else if(lolop->op.compare("AND") == 0)
						elim_cond(lolop, "PREV");	
						
					*(condlist_pool + count++) = search_duplicate(search_condition, condlist_pool);
				}
				else if(get_int(lolop->prev->value) < get_int(lolop->next->value))
				{
					if(lolop->op.compare("OR") == 0)
						elim_cond(lolop, "PREV");
					else if(lolop->op.compare("AND") == 0)
						elim_cond(lolop, "NEXT");
					
					*(condlist_pool + count++) = search_duplicate(search_condition, condlist_pool);
				}
			}
		}
		// < OR = , < AND =
		else if(lolop->prev->op.compare("<") == 0 && lolop->next->op.compare("=") == 0)
		{
			if(lolop->prev->type.compare("INT") == 0)
			{
				if(get_int(lolop->prev->value) > get_int(lolop->next->value))
				{
					if(lolop->op.compare("OR") == 0)	
						elim_cond(lolop, "NEXT");
					else if(lolop->op.compare("AND") == 0)	
						elim_cond(lolop, "PREV");
						
					*(condlist_pool + count++) = search_duplicate(search_condition, condlist_pool);
				}
			}
		}
		// = OR <
		else if(lolop->prev->op.compare("=") == 0 && lolop->next->op.compare("<") == 0)
		{
			if(lolop->prev->type.compare("INT") == 0)
			{
				if(get_int(lolop->prev->value) < get_int(lolop->next->value))
				{
					if(lolop->op.compare("OR") == 0)
						elim_cond(lolop, "PREV");
					else if(lolop->op.compare("AND") == 0)
						elim_cond(lolop, "NEXT");
						
					*(condlist_pool + count++) = search_duplicate(search_condition, condlist_pool);
				}
			}
		}
	}
}

void elim_cond(CONDLIST * lolop, string node)
{
	CONDLIST * temp = lolop;
	if(node.compare("PREV") == 0)
	{
		if(temp->prev->prev != NULL)
		{
			temp->prev->prev->next = temp->next;
			temp->next->prev = temp->prev->prev;
		}
		else
		{
			temp->next->prev = NULL;
		}
		
		free(temp->prev);
		free(temp);
	}
	else if(node.compare("NEXT") == 0)
	{
		if(temp->next->next != NULL)
		{
			temp->next->next->prev = temp->prev;
			temp->prev->next = temp->next->next;
		}
		else
		{
			temp->prev->next = NULL;
		}
		
		free(temp->next);
		free(temp);
	}
}

CONDLIST * search_duplicate(CONDLIST * search_condition, CONDLIST ** condlist_pool)
{
	CONDLIST	* temp = search_condition,
				* temp2;
			
	int 		i = 0;
	
	while(*(condlist_pool + i) != NULL)
	{
		temp2 = *(condlist_pool + i++);
		
		do
		{
			if(!is_equal(temp, temp2))
				return search_condition;
			temp2 = temp2->next;
			temp = temp->next;
		}while(temp != search_condition || temp2 != *(condlist_pool + i - 1));
		
		if(temp == search_condition || temp2 == *(condlist_pool + i - 1))
		{
			cout << "\n NULLS\n";
			return NULL;
		}

		temp = search_condition;		
		
	}
	
	
	return search_condition;
}