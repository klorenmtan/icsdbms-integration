
TABLEDATALIST **  update_op(UPDATEQUERYPARSE query)
{
	
	TABLEDATALIST 	** table = get_table(query.tablename), 
					** rows = NULL;
					
	string 			opTemp,
					column_nameTemp,
					typeTemp,
					opTemp2 = "";
					
	
	if(table == NULL)
		return false;
	else
	{
		if(query.search_condition == NULL)
			return false;
		else
		{
			rows = condlist_selector(table,query.search_condition);
			
			if(update_physical_operator(table, query.column_value, rows))
				return table;
			else
				return NULL;
		}
	}
	
		
}

bool update_physical_operator(TABLEDATALIST ** tabledatalist, DATA * column_value, TABLEDATALIST ** rows)
{

	TABLEDATALIST ** 	tabledataTemp1 = rows,
						* tabledataTemp2 = tabledatalist[0], 
						* firstRow = NULL;
						
	DATA * 				dataTemp1 = column_value;
	
	string 				column_name, 
						stringTemp1;
						
	int 				i=0;
	
	if(tabledatalist == NULL || rows == NULL)
	{
		return false;
	}
	
	for(int i = 0; *(rows + i) != NULL; i++)
	{
		//cout << "rows\n";
	}
	
	while(dataTemp1 != NULL)
	{	
		//locates the column to be updated
		
		do
		{
			stringTemp1 = *(static_cast<string *>(tabledataTemp2->data));
			if((*(static_cast<string *>(dataTemp1->data))).compare(stringTemp1) == 0)
				break;
			else
			{
				tabledataTemp2 = tabledataTemp2->next;
				for(i=0; *(tabledataTemp1+i) != NULL ; i++)
				{
					*(tabledataTemp1+i) = &(*(*(tabledataTemp1 + i))->next);
				}
			}
		}while(tabledataTemp2 != tabledatalist[0]);
		
		//locates the first data row
		firstRow = tabledataTemp2->down->down;
		
		do
		{
			for(i=0; *(tabledataTemp1+i) != NULL ; i++)
			{
				if(*(tabledataTemp1+i) == firstRow)
				{
					(*(tabledataTemp1+i))->data = dataTemp1->down->data;
					(*(tabledataTemp1+i))->type = dataTemp1->down->type;
				}	
				
			}
			firstRow = firstRow->down;		
		}while(firstRow != tabledataTemp2);
		
		dataTemp1 = dataTemp1->next;
		
	}
	
	return true;
}

