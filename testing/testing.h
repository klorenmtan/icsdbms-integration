SELECTQUERYPARSE build_sample_select()
{
	SELECTQUERYPARSE query;
	query.table = new DATA("VARCHAR", new string("EMPLOYEES"));
	query.search_condition = NULL;
	query.columnlist = new DATA("VARCHAR", new string("age"));
	query.columnlist->up = new DATA("VARCHAR", new string("Added age"));
	query.columnlist->down = new DATA("VARCHAR", new string("+"));
	query.columnlist->down->down = new DATA("INT", new int(100));
	query.order_grouping = new DATA("VARCHAR", new string("age"), NULL, NULL, NULL, new DATA("VARCHAR",new string("DESC")));
	return query;
}

TABLEDATALIST ** sample_join5()
{
	TABLEDATALIST ** rows;
	rows = join_op(*table_pool[0], *table_pool[1], 
	new CONDLIST("=","", NULL, "", new DATA("VARCHAR", new string("age")), new DATA("VARCHAR", new string("children"))), "FULL_JOIN");
	print_tabledatalist(rows);
	free_tabledatalist(rows);
}

TABLEDATALIST ** sample_join4()
{
	TABLEDATALIST ** rows;
	rows = join_op(*table_pool[0], *table_pool[1], 
	new CONDLIST("=","", NULL, "", new DATA("VARCHAR", new string("age")), new DATA("VARCHAR", new string("children"))), "RIGHT_JOIN");
	print_tabledatalist(rows);
	free_tabledatalist(rows);
}

TABLEDATALIST ** sample_join3()
{
	TABLEDATALIST ** rows;
	rows = join_op(*table_pool[0], *table_pool[1], 
	new CONDLIST("=","", NULL, "", new DATA("VARCHAR", new string("age")), new DATA("VARCHAR", new string("children"))), "LEFT_JOIN");
	print_tabledatalist(rows);
	free_tabledatalist(rows);
}


TABLEDATALIST ** sample_join2()
{
	TABLEDATALIST ** rows;
	rows = join_op(*table_pool[0], *table_pool[1], 
	new CONDLIST("<","", NULL, "", new DATA("VARCHAR", new string("age")), new DATA("VARCHAR", new string("children"))), "INNER_JOIN");
	print_tabledatalist(rows);
	free_tabledatalist(rows);
}

TABLEDATALIST ** sample_join()
{
	TABLEDATALIST ** rows;
	rows = join_op(*table_pool[0], *table_pool[1], new CONDLIST("<","", NULL, "", new DATA("VARCHAR", new string("age"), NULL, NULL, 
	new DATA("VARCHAR", new string("a"))), new DATA("VARCHAR", new string("age"), NULL, NULL, 
	new DATA("VARCHAR", new string("b")))), "INNER_JOIN");
	print_tabledatalist(rows);
	free_tabledatalist(rows);
}


CONDLIST * sample_condlist()
{
	return  new CONDLIST(">","age", new int(16), "INT");
}

CONDLIST * sample_condlist2()
{
	CONDLIST * search_condition;
	
	search_condition = new CONDLIST(">", "age", new int(17), "INT"); 
	search_condition->next = new CONDLIST("AND");
	search_condition->next->next = new CONDLIST(">", "age", new int(19), "INT");

	return search_condition;
}

DATA * build_sample_columnlist()
{
	DATA * columnlist, * temp1, * temp2;
	//columnlist = new DATA("VARCHAR",new string("age"));
	columnlist = new DATA("VARCHAR", new string("gender"));
	temp1 = new DATA("VARCHAR", new string("name"));
	columnlist->link(temp1);
	temp2 = new DATA("VARCHAR", new string("age"));
	temp1->link(temp2);
	//columnlist->link(temp1);
	//temp1->link(temp2);
	return columnlist;
}

UPDATEQUERYPARSE build_sample_update()
{
	UPDATEQUERYPARSE query;
	query.tablename = "EMPLOYEES";
	//query.search_condition = new CONDLIST("=","age", new int(18), "INT");
	query.search_condition = new CONDLIST("=","age", new int(18), "INT");
	//query.search_condition->next = new CONDLIST("OR");
	//query.search_condition->next->next = new CONDLIST("=","age", new int(20), "INT");
	//query.search_condition->next->next = new CONDLIST("LIKE", "name", new string("%solis%"), "VARCHAR");
	//query.search_condition->next->next->next = new CONDLIST("AND");
	//query.search_condition->next->next->next->next = new CONDLIST("LIKE", "name", new string("%a%"), "VARCHAR");
	query.column_value = new DATA("VARCHAR", new string("gender"), NULL, NULL, NULL, new DATA("VARCHAR", new string("S")));
	return query;

}

UPDATEQUERYPARSE build_sample_update2()
{
	UPDATEQUERYPARSE query;
	query.tablename = "EMPLOYEES";
	//query.search_condition = new CONDLIST("=","age", new int(18), "INT");
	query.search_condition = new CONDLIST("LIKE", "name", new string("%s"), "VARCHAR");
	query.column_value = new DATA("VARCHAR", new string("gender"), NULL, NULL, NULL, new DATA("VARCHAR", new string("S")));
	return query;
}

UPDATEQUERYPARSE build_sample_update3()
{
	UPDATEQUERYPARSE query;
	query.tablename = "EMPLOYEES";
	query.search_condition = new CONDLIST("LIKE", "name", new string("%olis%"), "VARCHAR");
	query.column_value = new DATA("VARCHAR", new string("gender"), NULL, NULL, NULL, new DATA("VARCHAR", new string("J")));
	return query;
}

UPDATEQUERYPARSE build_sample_update4()
{
	UPDATEQUERYPARSE query;
	query.tablename = "EMPLOYEES";
	query.search_condition = new CONDLIST("LIKE", "name", new string("_q%"), "VARCHAR");
	query.column_value = new DATA("VARCHAR", new string("gender"), NULL, NULL, NULL, new DATA("VARCHAR", new string("J")));
	return query;
}

UPDATEQUERYPARSE build_sample_update5()
{
	UPDATEQUERYPARSE query;
	query.tablename = "EMPLOYEES";
	query.search_condition = new CONDLIST("LIKE", "name", new string("[a]%"), "VARCHAR");
	query.column_value = new DATA("VARCHAR", new string("gender"), NULL, NULL, NULL, new DATA("VARCHAR", new string("X")));
	return query;
}

UPDATEQUERYPARSE build_sample_update6()
{
	UPDATEQUERYPARSE query;
	query.tablename = "EMPLOYEES";
	query.search_condition = new CONDLIST("<=", "age", new int(17), "INT");
	query.search_condition->next = new CONDLIST("AND");
	query.search_condition->next->next = new CONDLIST("=", "name", new string("tan"), "VARCHAR");
	query.column_value = new DATA("VARCHAR", new string("gender"), NULL, NULL, NULL, new DATA("VARCHAR", new string("Z")));
	return query;
}

UPDATEQUERYPARSE build_sample_update7()
{
	UPDATEQUERYPARSE query;
	query.tablename = "EMPLOYEES";
	query.search_condition = new CONDLIST("<=", "age", new int(17), "INT");
	query.search_condition->next = new CONDLIST("OR");
	query.search_condition->next->next = new CONDLIST("=", "age", new int(18), "INT");
	query.column_value = new DATA("VARCHAR", new string("gender"), NULL, NULL, NULL, new DATA("VARCHAR", new string("Z")));
	return query;
}

UPDATEQUERYPARSE build_sample_update8()
{
	UPDATEQUERYPARSE query;
	query.tablename = "EMPLOYEES";
	query.search_condition = new CONDLIST("ISNULL", "age", NULL, "");
	query.column_value = new DATA("VARCHAR", new string("gender"), NULL, NULL, NULL, new DATA("VARCHAR", new string("K")));
	return query;
}

DELETEQUERYPARSE build_sample_delete()
{
	DELETEQUERYPARSE query;
	query.tablename = "EMPLOYEES";
	//query.search_condition = new CONDLIST(">","age", new int(20), "INT");
	query.search_condition = new CONDLIST("=","name", new string("solis"), "VARCHAR");
	query.search_condition->next = new CONDLIST("OR");
	//query.search_condition->next = new CONDLIST("AND");
	//query.search_condition->next->next = new CONDLIST("=","age", new int(18), "INT");
	//query.search_condition->next->next->next = new CONDLIST("OR");
	//query.search_condition->next->next->next->next = new CONDLIST("=","age", new int(19), "INT");
	query.search_condition->next->next = new CONDLIST("LIKE", "gender", new string("%F%"), "VARCHAR");
	//query.search_condition->next->next->next = new CONDLIST("AND");
	//query.search_condition->next->next->next->next = new CONDLIST("LIKE", "name", new string("%a%"), "VARCHAR");
	//query.column_value = new DATA("VARCHAR", new string("gender"), NULL, NULL, NULL, new DATA("VARCHAR", new string("S")));
	return query;

}

INSERTQUERYPARSE build_sample_insert()
{
	/*	
		TABLEDATALIST * temp = new TABLEDATALIST(new string("tan"), "VARCHAR");
		temp->next = new TABLEDATALIST(new int(17), "INT");
		temp->next->next = new TABLEDATALIST(new string("F"), "VARCHAR");
		temp->next->next->next = temp;
	*/
	INSERTQUERYPARSE query;
	query.tablename = "EMPLOYEES";
	query.inserttype = "MANUAL";
	DATA * temp = new DATA("VARCHAR", new string("name"), NULL, NULL, NULL, new DATA("VARCHAR", new string("SANTOS")));
	temp->next = new DATA("VARCHAR", new string("gender"), NULL, NULL, NULL, new DATA("VARCHAR", new string("F")));
	query.datalist = temp;
	return query;
}

INSERTQUERYPARSE build_sample_insert2()
{
	
	INSERTQUERYPARSE query;
	query.tablename = "EMPLOYEES";
	query.inserttype = "MANUAL";
	DATA * temp = new DATA("VARCHAR", new string("name"), NULL, NULL, NULL, new DATA("VARCHAR", new string("aquino")));
	query.datalist = temp;
	return query;
}

INSERTQUERYPARSE build_sample_insert3()
{
	INSERTQUERYPARSE query;
	query.tablename = "EMPLOYEES";
	query.inserttype = "MANUAL";
	DATA * temp = new DATA("VARCHAR", new string("name"), NULL, NULL, NULL, new DATA("VARCHAR", new string("tan")));
	temp->next = new DATA("VARCHAR", new string("age"), NULL, NULL, NULL, new DATA("INT", new int(15)));
	temp->next->next = new DATA("VARCHAR", new string("gender"), NULL, NULL, NULL, new DATA("VARCHAR", new string("F")));
	query.datalist = temp;
	return query;
	
	
}


TABLE * build_test_table()
{
	TABLE * table =  new TABLE();
	int noOfRows = 8;
	int noOfColumns = 3;
	TABLEDATALIST  ** head, * temp1, * temp2;
	head = (TABLEDATALIST **)malloc(sizeof(TABLEDATALIST *)*11);


	//col name
	head[0] = new TABLEDATALIST(new string("name"), "VARCHAR");
	head[0]->next = new TABLEDATALIST(new string("age"), "VARCHAR");
	head[0]->next->next = new TABLEDATALIST(new string("gender"), "VARCHAR");
	head[0]->next->prev = head[0];
	head[0]->next->next->prev = head[0]->next;
	head[0]->next->next->next = head[0];
	head[0]->prev = head[0]->next->next;
	
	//cout << "Entered" << "\n";
		//::print_tabledatalist(head[0]);
	//end of col name

	//col type
	head[1] = new TABLEDATALIST(new string("VARCHAR"), "VARCHAR");
	head[1]->next = new TABLEDATALIST(new string("INT"), "VARCHAR", "PRIMARY");
	head[1]->next->next = new TABLEDATALIST(new string("VARCHAR"), "VARCHAR");
	head[1]->next->prev = head[1];
	head[1]->next->next->prev = head[1]->next;
	head[1]->next->next->next = head[1];
	head[1]->prev = head[1]->next->next;
		//::print_tabledatalist(head[1]);
	//end of col data type
	
		
	//populate data
	for(int i=2; i<noOfRows+2; i++)
	{
		head[i]= new TABLEDATALIST(new string("solisiss"+i-2), "VARCHAR");
		head[i]->next = new TABLEDATALIST(new int(i+15), "INT");
		if(i%3==0)
			head[i]->next->next = new TABLEDATALIST(new string("M"), "VARCHAR");
		else if(i%3==1)
			head[i]->next->next = new TABLEDATALIST(new string("F"), "VARCHAR");
		else
			head[i]->next->next = new TABLEDATALIST(new string("B"), "VARCHAR");
		head[i]->next->prev = head[i];
		head[i]->next->next->prev = head[i]->next;
		head[i]->next->next->next = head[i];
		head[i]->prev = head[i]->next->next;
		//print_tabledatalist(head[i]);
	}
	
	for(int i=0; i<noOfRows+2; i++)
	{
		if(i != noOfRows+1)
			head[i]->down=head[i+1];
		if(i != 0)
			head[i]->up=head[i-1];
		if(i == 0)
			head[i]->up = head[noOfRows+1];
		if(i == noOfRows+1)
			head[i] = head[0];
	}

	temp1 = temp2 = head[0];
	/*assigning the ->down to other cells except for the first column*/
	do{
		do{
			temp2->next->down = temp2->down->next;
			temp2 = temp2->down;
		}while(temp2->down != NULL);
		
		if(temp2->down == NULL)
		{
			temp2->down = temp1;
		}
		
		temp1 = temp1->next;
		temp2 = temp1;
		
	}while(temp1 != head[0]);
	
	
	temp1 = temp2 = head[0]->next;
	do{
		do{
			temp2->down->up = temp2;
			temp2 = temp2->down;
		}while(temp2->down != temp1);
		
		
		if(temp2->down == temp1)
		{
			temp1->up = temp2;
			//temp2->down = temp1;
		}
		
		temp1 = temp1->next;
		temp2 = temp1;
	}while(temp1->next!=head[0]->next);
		
		//::print_tabledatalist(head[0]->next->next);
	
	table->table = head;
	table->alias = "a";
	table->tablename = "EMPLOYEES";
	return table;
}

TABLE * build_test_table2()
{
	TABLE * table = new TABLE();
	int noOfRows = 4;
	int noOfColumns = 3;
	TABLEDATALIST  ** head, * temp1, * temp2;
	head = (TABLEDATALIST **)malloc(sizeof(TABLEDATALIST *)*6);

	//col name
	head[0] = new TABLEDATALIST(new string("name"), "VARCHAR");
	head[0]->next = new TABLEDATALIST(new string("age"), "VARCHAR");
	head[0]->next->next = new TABLEDATALIST(new string("gender"), "VARCHAR");
	head[0]->next->prev = head[0];
	head[0]->next->next->prev = head[0]->next;
	head[0]->next->next->next = head[0];
	head[0]->prev = head[0]->next->next;
	
	
	//cout << "Entered" << "\n";
		//::print_tabledatalist(head[0]);
	//end of col name

	//col type
	head[1] = new TABLEDATALIST(new string("VARCHAR"), "VARCHAR");
	head[1]->next = new TABLEDATALIST(new string("INT"), "VARCHAR", "PRIMARY");
	head[1]->next->next = new TABLEDATALIST(new string("VARCHAR"), "VARCHAR");
	head[1]->next->prev = head[1];
	head[1]->next->next->prev = head[1]->next;
	head[1]->next->next->next = head[1];
	head[1]->prev = head[1]->next->next;
		//::print_tabledatalist(head[1]);
	//end of col data type
	
	
	head[0]->down = head[1];
	head[1]->up = head[0];
	head[0]->next->down = head[1]->next;
	head[1]->next->up = head[0]->next;
	head[0]->next->next->down = head[1]->next->next;
	head[1]->next->next->up = head[0]->next->next;
	
	
	head[0]->up = head[1];
	head[1]->down = head[0];
	head[0]->next->up = head[1]->next;
	head[1]->next->down = head[0]->next;
	head[0]->next->next->up = head[1]->next->next;
	head[1]->next->next->down = head[0]->next->next;
	
	table->table = head;
	table->alias = "";
	table->tablename = "EMPLOYEES";
	return table;
}

TABLE * build_test_table3()
{
	TABLE * table =  new TABLE();
	int noOfRows = 2;
	int noOfColumns = 3;
	TABLEDATALIST  ** head, * temp1, * temp2;
	head = (TABLEDATALIST **)malloc(sizeof(TABLEDATALIST *)*6);

	//col name
	head[0] = new TABLEDATALIST(new string("name"), "VARCHAR");
	head[0]->next = new TABLEDATALIST(new string("children"), "VARCHAR");
	head[0]->next->next = new TABLEDATALIST(new string("gender"), "VARCHAR");
	head[0]->next->prev = head[0];
	head[0]->next->next->prev = head[0]->next;
	head[0]->next->next->next = head[0];
	head[0]->prev = head[0]->next->next;
	
	
	//cout << "Entered" << "\n";
		//::print_tabledatalist(head[0]);
	//end of col name

	//col type
	head[1] = new TABLEDATALIST(new string("VARCHAR"), "VARCHAR");
	head[1]->next = new TABLEDATALIST(new string("INT"), "VARCHAR", "PRIMARY");
	head[1]->next->next = new TABLEDATALIST(new string("VARCHAR"), "VARCHAR");
	head[1]->next->prev = head[1];
	head[1]->next->next->prev = head[1]->next;
	head[1]->next->next->next = head[1];
	head[1]->prev = head[1]->next->next;
		//::print_tabledatalist(head[1]);
	//end of col data type
		
	//populate data
	for(int i=2; i<noOfRows+2; i++)
	{
		head[i]= new TABLEDATALIST(new string("tiago"+i-2), "VARCHAR");
		head[i]->next = new TABLEDATALIST(new int(i+18), "INT");
		if(i%2==0)
			head[i]->next->next = new TABLEDATALIST(new string("M"), "VARCHAR");
		else
			head[i]->next->next = new TABLEDATALIST(new string("F"), "VARCHAR");
		head[i]->next->prev = head[i];
		head[i]->next->next->prev = head[i]->next;
		head[i]->next->next->next = head[i];
		head[i]->prev = head[i]->next->next;
		//print_tabledatalist(head[i]);
	}
	
	for(int i=0; i<noOfRows+2; i++)
	{
		if(i != noOfRows+1)
			head[i]->down=head[i+1];
		if(i != 0)
			head[i]->up=head[i-1];
		if(i == 0)
			head[i]->up = head[noOfRows+1];
		if(i == noOfRows+1)
			head[i] = head[0];
	}

	temp1 = temp2 = head[0];
	/*assigning the ->down to other cells except for the first column*/
	do{
		do{
			temp2->next->down = temp2->down->next;
			temp2 = temp2->down;
		}while(temp2->down != NULL);
		
		if(temp2->down == NULL)
		{
			temp2->down = temp1;
		}
		
		temp1 = temp1->next;
		temp2 = temp1;
		
	}while(temp1 != head[0]);
	
	
	temp1 = temp2 = head[0]->next;
	do{
		do{
			temp2->down->up = temp2;
			temp2 = temp2->down;
		}while(temp2->down != temp1);
		
		
		if(temp2->down == temp1)
		{
			temp1->up = temp2;
			//temp2->down = temp1;
		}
		
		temp1 = temp1->next;
		temp2 = temp1;
	}while(temp1->next!=head[0]->next);
		
		//::print_tabledatalist(head[0]->next->next);
	
	table->table = head;
	table->alias = "b";
	table->tablename = "EMP";
	return table;
}

SELECTQUERYPARSE build_test_query()
{
	/*
	SELECTQUERYPARSE * query = new QUERYPARSE();
	query->type.assign("SELECT");
	cout << query->type << "\n";
	
	SELLIST * sellist = new SELLIST();
	ATTRIBUTE attribute;
	attribute.name.assign("name");
	sellist->attribute = attribute;
	sellist->next = NULL;
	sellist->prev = NULL; 
	//cout << attribute.name << "\n";
	query->sellist = sellist;
	
	CONDLIST * condlist = new CONDLIST();
	
	COND cond;
	cond.attribute.assign("age");
	cond.op.assign(">");
	cond.value = 18;
	//cout << cond.attribute << cond.op << "\n";
	
	condlist->cond = cond;
	condlist->prev = NULL;
	condlist->next = NULL;
	
	query->condlist = condlist;
	
	TABLELIST  * tablelist = new TABLELIST();
	
	TABLE table;
	table.name.assign("employees");
	table.metadata = NULL;
	//cout << table.name << "\n";
	
	tablelist->table = table;
	tablelist->prev = NULL;
	tablelist->next = NULL;
	
	query->tablelist = tablelist;
	return *query;	
	*/
}


