TABLEDATALIST ** insert_op(INSERTQUERYPARSE query)
{
	
	TABLEDATALIST 			** table = get_table(query.tablename),
							* datalist = NULL;
							
	DATA					* data = query.datalist;
	
	
	if(table == NULL)
		return NULL;
	else
	{
		datalist = build_datalist(get_table_header(table), data);
		return insert_physical_operator(table, datalist, query.inserttype);
	}
}

TABLEDATALIST **  insert_physical_operator(TABLEDATALIST ** table, TABLEDATALIST * querydata, string inserttype)
{
	if(inserttype.compare("MANUAL") == 0)
	{
		TABLEDATALIST * temp1 = table[0]->up,
				* temp2 = querydata,
				* temp3 = table[1], 
				* temp4 = querydata, 
				* temp5 = NULL;
		
		string strTemp1;
		
		/*checking of CONSTRAINTS UNIQUE and PRIMARY*/
		do
		{
			strTemp1 = temp3->constraint;
			if(strTemp1.compare("UNIQUE") == 0 || strTemp1.compare("PRIMARY") == 0)
			{
				temp5 = temp3->down;
				do
				{
					if(temp4->type.compare("STRING") == 0)
					{
						if((*(static_cast<string *>(temp5->data))).compare(*(static_cast<string *>(temp4->data))) == 0)
						{
							return NULL;	
						}
					}
					else if(temp4->type.compare("INT") == 0)
					{
						if(*(static_cast<int *>(temp5->data)) == *(static_cast<int *>(temp4->data)))
						{
							return NULL;	
						}
					}
					temp5 = temp5->down;
				}while(temp5 != temp3);
			}
			temp3 = temp3->next;
			temp4 = temp4->next;
		}while(temp3 != table[1]);
		
		temp5 = temp2;
		
		do
		{
			temp2->down = temp1->down;
			temp1->down = temp2;
			temp2->up = temp1;
			temp2->down->up = temp2;
			temp2 = temp2->next;
			temp1 = temp1->next;
			temp2->prev = temp1->prev->down;
			
		}while(temp2 != temp5);
		
	
		return table;
	}
}

TABLEDATALIST * build_datalist(TABLEDATALIST ** header, DATA * datalist)
{
	TABLEDATALIST 	* temp = header[0],
					* temp3 = NULL,
					* newdata = NULL,
					* previousdata = NULL;
	
	DATA			* temp2 = NULL;
	
	bool boolTemp = false;
	
	do
	{
		boolTemp = false;
		temp2 = datalist;
		
		do
		{
			if(get_string(temp2->data).compare(get_string(temp->data)) == 0)
			{
				boolTemp = true;
				break;
			}
				
			temp2 = temp2->next;
			
		}while(temp2 !=NULL);
		
		if(boolTemp)
		{
			if(previousdata == NULL)
			{
				newdata = new TABLEDATALIST(temp2->down->data, temp2->down->type);
				previousdata = newdata;
			}
			else
			{
				previousdata->next = new TABLEDATALIST(temp2->down->data, temp2->down->type);
				previousdata = previousdata->next;
			}
		}
		else
		{
			
			if(previousdata == NULL)
			{
				newdata->data = NULL;
				previousdata = newdata;
			}
			else
			{
				previousdata->next = new TABLEDATALIST(NULL,"");
				previousdata = previousdata->next;
			}
		}
		
		temp = temp->next;
	}while(temp != header[0]);
	
	previousdata->next = newdata;
	//print_data(newdata->next->next->next);
	
	return newdata;
	

}



