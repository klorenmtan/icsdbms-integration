TABLEDATALIST ** select_op(SELECTQUERYPARSE query)
{
	TABLEDATALIST 	** table, 
					** table2, 
					** rows = NULL,
					** select_table = NULL;
					
	CONDLIST 		* condlist = query.search_condition;
	
	string 			opTemp,
					column_nameTemp,
					typeTemp,
					opTemp2 = "";
					
	if(query.table->next == NULL)
		table = get_table(get_string(query.table));
	else
	{
		if(get_string(query.table->next).compare("INNER_JOIN") == 0)
		{
			table  = get_table(get_string(query.table));
			table2 = get_table(get_string(query.table->next->next));
		}
	}
	
	if(table == NULL)
		return false;
	else
	{
		rows = condlist_selector(table, query.search_condition);
		select_table = create_new_tabledatalist(table, rows, query.columnlist);
		//circular_test(table);
		select_display_op(select_table, query.columnlist, query.group_grouping);
		
		/*
		DATA * display_constraint;
		DATA * group_grouping;
		DATA * group_condition;
		DATA * order_grouping;
		*/
	}
	print_tabledatalist(select_table);
	return select_table;
}

bool select_display_op(TABLEDATALIST ** tabledatalist, DATA * columnlist, DATA * group_grouping)
{
	TABLEDATALIST 	** table = tabledatalist,
					* tabledataTemp = tabledatalist[0];
					
	
	DATA 			* tempColList = columnlist,
					* dataTemp;
	
	
	do
	{
		cout << "Entered";
		if(get_string(tempColList->data).compare(convert_to_string(tabledataTemp)) == 0)
		{
			dataTemp = tempColList;
			while(dataTemp->down != NULL)
			{
				if(select_display_alteration(tabledataTemp, get_string(dataTemp->down->data), dataTemp->down->down, group_grouping, table))
					dataTemp = dataTemp->down->down;
				else
					dataTemp = dataTemp->down;
			}	
		}
		select_alias(tabledataTemp, tempColList);
		
		tabledataTemp = tabledataTemp->next;
		tempColList = tempColList->next;
		
	}while(tempColList != NULL);
}

bool select_display_alteration(TABLEDATALIST * column, string op, DATA * value, DATA * group_grouping, TABLEDATALIST ** table)
{
	TABLEDATALIST 	* temp = column->down->down;
	
	int 			intTemp;
	
	if(op.compare("+") == 0 || op.compare("-") == 0 || op.compare("*") == 0 || op.compare("/") == 0)
	{
			intTemp = get_int(value->data);
			do
			{
				if(temp->type.compare("INT") == 0)
				{
					if(op.compare("+") == 0)
						temp->data = new int(get_int(temp->data) + intTemp);
					else if(op.compare("-") == 0)
						temp->data = new int(get_int(temp->data) - intTemp);
					else if(op.compare("*") == 0)
						temp->data = new int(get_int(temp->data) * intTemp);
					else if(op.compare("/") == 0)
						temp->data = new int(get_int(temp->data) / intTemp);
				}
				else 
					temp->data = NULL;
				temp = temp->down;
			}while(temp != column);
		return true;
	}
	else if(op.compare("SUM") == 0)
	{
		
		return false;
	}
}
	

bool select_alias(TABLEDATALIST * column, DATA * tempColList)
{
	if(tempColList->up == NULL)
		return false;
	else
		column->data = new string(get_string(tempColList->up->data));
}
