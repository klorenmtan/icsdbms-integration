void print_row(TABLEDATALIST * row)
{
	TABLEDATALIST * temp1 = row;
	cout << "\n";
	do
	{
		if(temp1->data == NULL)
		{
			cout << "NULL \t";
		}
		else if(temp1->type.compare("VARCHAR")==0)
		{
			cout << *(static_cast<string *>(temp1->data)) << "\t";
		}
		else if(temp1->type.compare("INT")==0)
		{
			cout << *(static_cast<int *>(temp1->data)) << "\t";
		}
		else if(temp1->type.compare("BOOLEAN")==0)
		{
			if(*(static_cast<bool *>(temp1->data)) == false)
				cout << "0\t";
			else
				cout << "1\t";
		}
		else if(temp1->type.compare("FLOAT")==0)
		{
			printf("%4f \t", *(static_cast<float *>(temp1->data)));
		}
		else if(temp1->type.compare("DATE")==0)
		{
			printf("%02d \t", (*(static_cast<DATE *>(temp1->data))).month);
			printf("%02d \t", (*(static_cast<DATE *>(temp1->data))).day);
			printf("%02d \t", (*(static_cast<DATE *>(temp1->data))).year);
		}
		
		temp1 = temp1->next;	
		cout << "\n";
		
	}while(temp1 != row);
}

void print_column(TABLEDATALIST * column)
{
	TABLEDATALIST * temp1 = column;
	cout << "\n";
	do
	{
		if(temp1->data == NULL)
		{
			cout << "NULL \t";
		}
		else if(temp1->type.compare("VARCHAR")==0)
		{
			cout << *(static_cast<string *>(temp1->data)) << "\t";
		}
		else if(temp1->type.compare("INT")==0)
		{
			cout << *(static_cast<int *>(temp1->data)) << "\t";
		}
		else if(temp1->type.compare("BOOLEAN")==0)
		{
			if(*(static_cast<bool *>(temp1->data)) == false)
				cout << "0\t";
			else
				cout << "1\t";
		}
		else if(temp1->type.compare("FLOAT")==0)
		{
			printf("%4f \t", *(static_cast<float *>(temp1->data)));
		}
		else if(temp1->type.compare("DATE")==0)
		{
			printf("%02d \t", (*(static_cast<DATE *>(temp1->data))).month);
			printf("%02d \t", (*(static_cast<DATE *>(temp1->data))).day);
			printf("%02d \t", (*(static_cast<DATE *>(temp1->data))).year);
		}
		
		temp1 = temp1->down;	
		cout << "\n";
		
	}while(temp1 != column);
}

void print_data(TABLEDATALIST * data)
{
	if(data->type.compare("VARCHAR") == 0)
		cout << "Print data: \t"<<*(static_cast<string *>(data->data)) << "\n";
	else if(data->type.compare("INT") == 0)
		cout << "Print data: \t"<<*(static_cast<int *>(data->data)) << "\n";
}

void print_data(DATA * data)
{
	if(data->type.compare("VARCHAR") == 0)
		cout << "Print data: \t"<<*(static_cast<string *>(data->data)) << "\n";
	else if(data->type.compare("INT") == 0)
		cout << "Print data: \t"<<*(static_cast<int *>(data->data)) << "\n";
}


bool print_tabledatalist(TABLEDATALIST ** tabledatalist)
{
	/*printing of the content of the tabledatalist*/
	TABLEDATALIST 		* temp1,
						* temp2;
						
	int 				count = 0;
	
	//cout << convert_to_string(tabledatalist[0]) << "string";
	if (tabledatalist == NULL || tabledatalist[0] == NULL)
	{
		cout << "TABLE EMPTY";
		return false;
	}
	temp2 = temp1 = tabledatalist[0];
	cout << "\n";
	do{
		if(temp1->up != tabledatalist[0])
		{
			do
			{
				if(temp1->data == NULL)
				{
					cout << "NULL \t";
				}
				else if(temp1->type.compare("VARCHAR")==0 || temp1->type.compare("CHAR")==0)
				{
					cout << *(static_cast<string *>(temp1->data)) << "\t";
				}
				else if(temp1->type.compare("INT")==0)
				{
					cout << *(static_cast<int *>(temp1->data)) << "\t";
				}
				else if(temp1->type.compare("BOOLEAN")==0)
				{
					cout << *(static_cast<bool *>(temp1->data)) << "\t";
				}
				else if(temp1->type.compare("FLOAT")==0)
				{
					printf("%2f \t", *(static_cast<float *>(temp1->data)));
					
				}
				temp1 = temp1->next;	
			}while(temp1!=temp2);
			cout << "\n";	
		}
		temp2 = temp2->down;
		temp1 = temp2;		
	//}while(temp2 != NULL);
	}while(temp2 != tabledatalist[0]);
	
	count = row_count(temp2);
	if(count > 1)
		cout << "\n" << count << " rows in set\n";
	else
	{
		cout << "\n" << count << " row in set\n";
	}
	
	return true;
}
