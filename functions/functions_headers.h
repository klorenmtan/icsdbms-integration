TABLEDATALIST ** get_table(string tablename);
bool print_tabledatalist(TABLEDATALIST ** tabledatalist);
void print_data(TABLEDATALIST * data);
void print_data(DATA * data);
bool circular_test(TABLEDATALIST ** tabledatalist);

//functions/character_functions.h
bool				substr						(	TABLEDATALIST * column,
													int pos,
													int len
												);
bool 				replace						(	TABLEDATALIST * column,
													string tosearchfor,
													string replacewiththis 
												);
TABLEDATALIST *		 length						(	TABLEDATALIST * column
												);
bool 				initcap						(	TABLEDATALIST * column
												);
TABLEDATALIST * 	concat						(	TABLEDATALIST * column1,
													TABLEDATALIST * column2,
													string newcolumname
												);
bool 				upper						(	TABLEDATALIST * column
												);
bool 				lower						(	TABLEDATALIST * column
												);

//functions/functions_headers.h
bool 				lower						(	TABLEDATALIST * column
												);
bool 				upper						(	TABLEDATALIST * column
												);
//table/table.h
TABLEDATALIST * 	find_row_head				(	TABLEDATALIST ** table,
													TABLEDATALIST * cell
												);
bool 				create_new_cell				(	void * data,
													string type,
													TABLEDATALIST * next,
													TABLEDATALIST * prev,
													TABLEDATALIST * up,
													TABLEDATALIST * down
												);
TABLEDATALIST * 	make_row_data_null			(	TABLEDATALIST ** table,
													string column,
													TABLEDATALIST * end = NULL
												);
void 				connect_two_rows_horizontal(	TABLEDATALIST * row,
													TABLEDATALIST * row2
												);
void 				connect_two_rows_vertical	(	TABLEDATALIST * row,
													TABLEDATALIST * row2
												);
TABLEDATALIST *		 find_row_end				(	TABLEDATALIST * row
												);
TABLEDATALIST * 	copy_row					(	TABLEDATALIST * row
												);
TABLEDATALIST * 	find_column					(	TABLEDATALIST ** table,
													string column,
													TABLEDATALIST * end
												);
 bool 				remove_column				(	TABLEDATALIST ** table,
													DATA * columnlist
												);
 TABLEDATALIST ** 	get_table_header			(	TABLEDATALIST ** table
												);
 TABLEDATALIST ** 	create_new_tabledatalist	(	TABLEDATALIST ** table,
													TABLEDATALIST ** rows,
													DATA * columnlist
												);
 TABLEDATALIST ** 	get_table					(	string tablename
												);
 
//data/data_conversion.h
bool 				is_equal					(	TABLEDATALIST * cell1,
													TABLEDATALIST * cell2
												);
bool 				is_equal					(	CONDLIST * node1,
													CONDLIST * node2
												);
bool 				is_void_equal				(	string type,
													void * data,
													void * data2
												);
string 				get_string					(	void * data
												);
int 				get_int						(	void * data
												);
float 				get_float					(	void * data
												);
int 				get_boolean					(	void * data
												);
string 				convert_to_string			(	TABLEDATALIST * temp
												);


//physical_operators/update_physical_operator.h
TABLEDATALIST **  	update						(	UPDATEQUERYPARSE query	
												);
bool 				update_physical_operator	(	TABLEDATALIST ** tabledatalist,
													DATA * column_value,
													TABLEDATALIST ** rows	
												);
										
//physical_operators/delete_physical_operator.h
TABLEDATALIST ** 	delete_op					(	DELETEQUERYPARSE query
												);
bool 				delete_physical_operator	(	TABLEDATALIST ** tabledatalist,
													TABLEDATALIST ** rows
												);
												
//physical_operators/select_physical_operator.h
TABLEDATALIST ** 	select_op					(	SELECTQUERYPARSE query
												);
bool 				select_display_op			(	TABLEDATALIST ** tabledatalist,
													DATA * columnlist,
													DATA * group_grouping
												);
bool 				select_display_alteration	(	TABLEDATALIST *column,
													string op, 
													DATA * value,
													DATA * group_grouping,
													TABLEDATALIST ** table
												);
bool 				select_alias				(	TABLEDATALIST * column,
													DATA * tempColList
												);

												
//selector/condlist_selector.h
TABLEDATALIST ** 	condlist_selector			(	TABLEDATALIST ** table, 
													CONDLIST * search_condition,
													TABLEDATALIST * column_search,
													TABLEDATALIST * row_search
												);
int 				cond_stat					(	
													TABLEDATALIST ** table, 
													CONDLIST * search_condition
												);
												
//selector/selector.h
TABLEDATALIST ** 	selector					(	TABLEDATALIST ** table,
													string colname, 
													string op,
													TABLEDATALIST ** rows, 
													string lolop,
													TABLEDATALIST * column_search,
													TABLEDATALIST * row_search
												);
TABLEDATALIST ** 	selector					(	TABLEDATALIST ** table,
													string colname, 
													string op,
													string * value, 
													TABLEDATALIST ** rows, 
													string lolop, 
													TABLEDATALIST * column_search,
													TABLEDATALIST * row_search
												);
TABLEDATALIST ** 	selector					(	TABLEDATALIST ** table, 
													string colname,
													string type,
													void * lower,
													bool lBound,
													void * upper,
													bool uBound,
													TABLEDATALIST ** rows,
													string lolop,
													TABLEDATALIST * column_search,
													TABLEDATALIST * row_search
												);
TABLEDATALIST ** 	selector					(	TABLEDATALIST ** table
												);	
												
//physical_operators/insert_physical_operator.h
TABLEDATALIST ** 	insert_op					(	INSERTQUERYPARSE query
												);
TABLEDATALIST **  	insert_physical_operator	(	TABLEDATALIST ** table,
													TABLEDATALIST * querydata,
													string inserttype
												);
TABLEDATALIST * 	build_datalist				(	TABLEDATALIST ** header, 
													DATA * datalist
												);

//optimization/pattern_matching.h
void 					double_linked_list		(	CONDLIST * search_condition
												);
int  					count_lolop				(	CONDLIST * search_condition
												);
CONDLIST **				get_lolop				(	CONDLIST * search_condition
												);
void 					switch_cond				(	CONDLIST * prev,
													CONDLIST * next
												);
CONDLIST * 				recopy_cond				(	CONDLIST * condlist
												);
CONDLIST * 				locate_current_lolop	(	CONDLIST * lolop,
													CONDLIST * search_condition,
													CONDLIST * new_search_condition
												);
void 					match_pattern			(	TABLEDATALIST ** table,
													CONDLIST * search_condition
												);
void 					pattern					(	CONDLIST * search_condition
												);

//optimization/patterns.h
CONDLIST * 				search_duplicate		(	CONDLIST * condlist,
													CONDLIST ** condlist_pool
												);
void 					elim_or_pattern			(	TABLEDATALIST ** table,
													CONDLIST * lolop,
													CONDLIST * condlist,
													CONDLIST ** condlist_pool
												);
void 					swap_or_pattern			(	TABLEDATALIST ** table,
													CONDLIST * lolop,
													CONDLIST * condlist,
													CONDLIST ** condlist_pool
												);
void 					swap_and_pattern		(	TABLEDATALIST ** table,
													CONDLIST * lolop,
													CONDLIST * condlist,
													CONDLIST ** condlist_pool
												);
void 					elim_or_and_pattern		(	TABLEDATALIST ** table,
													CONDLIST * lolop,
													CONDLIST * search_condition,
													CONDLIST ** condlist_pool
												);
void 					elim_cond				(	CONDLIST * lolop, 
													string node
												);

//bpt/integrated_bpt.h
TABLEDATALIST 			** sort_desc			(	node * root,
													TABLEDATALIST ** table
												);
TABLEDATALIST 			** sort_asc				(	node * root,
													TABLEDATALIST ** table
												);
node 					* build_bpt_index		(	TABLEDATALIST ** table,
													TABLEDATALIST * column
												);
node 					* bpt_index_exists		(	string tablename, 
													string columname
												);
node 					* insert_to_bpt_pool	(	string table_name,
													string column_name, 
													node * root
												);

//statistical_data.h
void 					* column_min			(	string table_name,
													string column_name													
												);
void 					* column_max			(	string table_name,
													string column_name													
												);
int 					row_count				(	TABLEDATALIST ** table,
													string colname,
													string op,
													string * value
												);
int 					row_count				(	TABLEDATALIST ** table,
													string colname,
													string type,
													void * lower,
													bool lBound,
													void * upper,
													bool uBound
												);
int 					row_count				(	TABLEDATALIST ** table,
													string colname, 
													string op,
													TABLEDATALIST ** rows, 
													string lolop
												);
int 					row_count				(	TABLEDATALIST * table
												);
int 					col_count				(	TABLEDATALIST ** table
												);
//physical_operators/join_op.h
TABLEDATALIST ** 		full_join				(	TABLEDATALIST ** join_table,
													TABLEDATALIST * column,
													TABLEDATALIST * column2,
													string cond_op,
													int row_count_table1,
													int row_count_table2, 
													TABLEDATALIST * table2_start
												);
TABLEDATALIST ** 		right_join				(	TABLEDATALIST ** join_table,
													TABLEDATALIST * column,
													TABLEDATALIST * column2,
													string cond_op,
													int row_count_table1, 
													int row_count_table2, 
													TABLEDATALIST * table2_start
												);
TABLEDATALIST ** 		left_join				(	TABLEDATALIST ** join_table,
													TABLEDATALIST * column,
													TABLEDATALIST * column2,
													string cond_op,
													int row_count_table2, 
													TABLEDATALIST * table2_start
												);
TABLEDATALIST ** 		join_op					(	TABLE table,
													TABLE table2,
													CONDLIST * cond,
													string table_op
												);
TABLEDATALIST **		inner_join				(	TABLEDATALIST ** join_table,
													TABLEDATALIST * column,
													TABLEDATALIST * column2,
													string op
												);
TABLEDATALIST * 		get_other_table			(	TABLEDATALIST ** table
												);
TABLEDATALIST ** 		join					(	TABLEDATALIST ** table,
													TABLEDATALIST ** table2
												);
TABLEDATALIST **		inner_join_op			(	TABLE table,
													TABLE table2,
													CONDLIST * cond
												);
						 						
//printing functions
bool					free_tabledatalist		(	TABLEDATALIST ** tabledatalist
												);

//physical_operators/sum_aggregate_operator.h
void * 					sum_aggregate_operator	(	TABLEDATALIST ** table,
													TABLEDATALIST * column,
													TABLEDATALIST * group_grouping
												);
void * 					sum_op					(	TABLEDATALIST ** table,
													TABLEDATALIST * start,
													TABLEDATALIST * end,
													TABLEDATALIST * column
												);

//physical_operators/count_aggregate_operator.h
void * 					count_aggregate_operator(	TABLEDATALIST ** table,
													TABLEDATALIST * column,
													TABLEDATALIST * group_grouping
												);
void * 					count_op				(	TABLEDATALIST ** table,
													TABLEDATALIST * start,
													TABLEDATALIST * end,
													TABLEDATALIST * column
												);
//physical_operators/avg_aggregate_operator.h
void * 					sum_aggregate_operator	(	TABLEDATALIST ** table,
													TABLEDATALIST * column,
													TABLEDATALIST * group_grouping
												);
void * 					avg_op					(	TABLEDATALIST ** table,
													TABLEDATALIST * start,
													TABLEDATALIST * end,
													TABLEDATALIST * column
												);
												
//physical_operators/first_aggregate_operator.h
void * 					first_aggregate_operator(	TABLEDATALIST ** table,
													TABLEDATALIST * column,
													TABLEDATALIST * group_grouping
												);
void * 					first_op				(	TABLEDATALIST ** table, 
													TABLEDATALIST * start, 
													TABLEDATALIST * end, 
													TABLEDATALIST * column
												);

//physical_operators/last_aggregate_operator.h
void * 					last_aggregate_operator	(	TABLEDATALIST ** table,
													TABLEDATALIST * column,
													TABLEDATALIST * group_grouping
												);
void * 					last_op					(	TABLEDATALIST ** table, 
													TABLEDATALIST * start, 
													TABLEDATALIST * end, 
													TABLEDATALIST * column
												);

//physical_operators/max_aggregate_operator.h
void * 					max_aggregate_operator	(	TABLEDATALIST ** table,
													TABLEDATALIST * column,
													TABLEDATALIST * group_grouping
												);
void * 					max_op					(	TABLEDATALIST ** table, 
													TABLEDATALIST * start, 
													TABLEDATALIST * end, 
													TABLEDATALIST * column
												);
												
//physical_operators/max_aggregate_operator.h
void * 					min_aggregate_operator	(	TABLEDATALIST ** table,
													TABLEDATALIST * column,
													TABLEDATALIST * group_grouping
												);
void * 					min_op					(	TABLEDATALIST ** table, 
													TABLEDATALIST * start, 
													TABLEDATALIST * end, 
													TABLEDATALIST * column
												);