class RELALG
{
	public:
		string type;
		string tablename;
		CONDLIST * condlist;
		string attribute;
};

class RELALGLIST
{
	public:
		RELALG * inner;
		RELALG * outer;
		RELALG relalg;
};

class RELALGNODE_LL
{
	public:
		RELALGLIST node;
		RELALGLIST * next;
		RELALGLIST * prev;
};

class DATASET
{

};

class PLAN
{
	public:
		void * data;
};

class PLAN_LL
{
	public:
		PLAN plan;
		PLAN * next;
		PLAN * prev; 
};

class BTREE
{
	
};
