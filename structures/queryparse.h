typedef struct node {
	void ** pointers;
	int * keys;
	struct node * parent;
	bool is_leaf;
	int num_keys;
	struct node * next; // Used for queue.
} node;

class BPT
{
	public:
		BPT(string newtable_name, string newcolumn_name, node * newroot)
		{
			root = newroot;
			table_name = newtable_name;
			column_name = newcolumn_name;
		}
		
	node * root;
	string table_name;
	string column_name;
};

class DATE
{
	public:
		DATE(DATE * newdate)
		{
			month = newdate->month;
			day = newdate->day;
			year = newdate->year;
		}
		
		DATE(int newmonth, int newday, int newyear)
		{
			month = newmonth;
			day = newday;
			year = newyear;
		}
		
		int month;
		int day;
		int year;
};

class DATA
{
	public:
	
		DATA(DATA * old)
		{
			type = old->type;
			if(old->type.compare("INT") == 0)
				data = new int(*((int *)(old->data)));
			else if(old->type.compare("VARCHAR") == 0)
				data = new string(*((string *)(old->data)));
			else if(old->type.compare("BOOLEAN") == 0)
				data = new bool(*((bool *)(old->data)));
			else if(old->type.compare("FLOAT") == 0)
				data = new float(*((float *)(old->data)));
			else if(old->type.compare("DATE") == 0)
				data = new DATE(*((DATE *)(old->data)));
			
		}
	
		DATA(string newType, void * newData, DATA * newnext=NULL, DATA * newprev = NULL, DATA * newup = NULL, DATA * newdown = NULL)
		{
			data = newData;
			type = newType;
			
			if(newnext!= NULL)
				next = newnext;
			else
				next = NULL;
				
			if(newprev!= NULL)
				prev = newprev;
			else
				prev = NULL;
				
			if(newup!= NULL)
				up = newup;
			else
				next = NULL;
				
			if(newdown!= NULL)
				down = newdown;
			else
				down = NULL;
		}
		
		void link(DATA * second)
		{
			next = second;
			second->prev = this;
		}
		
		void * data;
		string type;
		DATA * next;
		DATA * prev;	
		DATA * up;
		DATA * down;
};

class TABLEDATALIST
{
	public:
		TABLEDATALIST(	void * newdata,
						string newtype= "", 
						string newconstraint = "", 
						TABLEDATALIST * newnext = NULL, 
						TABLEDATALIST * newprev = NULL,
						TABLEDATALIST * newup = NULL,
						TABLEDATALIST * newdown = NULL
		)
		{
			next = newnext;
			prev = newprev;
			up = newup;
			down = newdown;
			data = newdata;
			type = newtype;	
			constraint = newconstraint;
		};
		
		TABLEDATALIST(TABLEDATALIST * newtbl)
		{
			if(newtbl->type.compare("VARCHAR") == 0)
			{
				
				type = "VARCHAR";
				if(newtbl->data != NULL)
					data = new string(*(static_cast<string *>(newtbl->data)));
				else
					data = NULL;
					
				constraint = newtbl->constraint;
			}
			else if(newtbl->type.compare("INT") == 0)
			{
				type = "INT";
				if(newtbl->data != NULL)
					data = new int(*(static_cast<int *>(newtbl->data)));
				else
					data = NULL;
				constraint = newtbl->constraint;
			}
			else if(newtbl->type.compare("BOOL") == 0)
			{
				type = "BOOL";
				if(newtbl->data != NULL)
					data = new int(*(static_cast<bool *>(newtbl->data)));
				else
					data = NULL;
				constraint = newtbl->constraint;
			}
			else if(newtbl->type.compare("DATE") == 0)
			{
				type = "DATE";
				if(newtbl->data != NULL)
					data = new DATE(*(static_cast<DATE *>(newtbl->data)));
				else
					data = NULL;
				constraint = newtbl->constraint;
			}
			else if(newtbl->type.compare("CHAR") == 0)
			{
				type = "CHAR";
				if(newtbl->data != NULL)
					data = new int(*(static_cast<char *>(newtbl->data)));
				else
					data = NULL;
				constraint = newtbl->constraint;
			}
			else if(newtbl->type.compare("") == 0)
			{
				type = "";
				data = NULL;
				constraint = newtbl->constraint;
			}
			next = NULL;
			prev = NULL;
			up = NULL;
			down = NULL;
		};
		
		void update(void * newdata, string newtype= "", string newconstraint = "")
		{
			data = newdata;
			type = newtype;	
			constraint = newconstraint;
		}
		
		void * data;
		string type;
		string constraint;	
		TABLEDATALIST * prev;
		TABLEDATALIST * next;	
		TABLEDATALIST * up;
		TABLEDATALIST * down;
		
};


class METADATA
{
	void * data;
}METADATA;

class STATSDATA
{
	int no_of_rows;
	
}STATSDATA;

class COLLIST
{
	string colname;
	COLLIST * next;
	COLLIST * prev;
};

class TABLELIST
{
	string tablename;
	TABLELIST * next;
	TABLELIST * prev;
};

class CONDLIST
{
	public:
		CONDLIST()
		{
		}
	
		CONDLIST(string newop, string newcolumn_name = "",  void * newvalue = NULL , string newtype = "", DATA * newcolumn1 = NULL, DATA * newcolumn2 = NULL)
		{
			column_name = newcolumn_name;
			op = newop;
			value = newvalue;
			type = newtype;
			next = NULL;
			prev = NULL;
			column1 = newcolumn1;
			column2 = newcolumn2;
		}
		
		CONDLIST(CONDLIST * old)
		{
			column_name = old->column_name;
			op = old->op;
			type = old->type;
			
			if(old->type.compare("INT") == 0)
				value = new int(*((int *)(old->value)));
			else if(old->type.compare("VARCHAR") == 0)
				value = new string(*((string *)(old->value)));
			else if(old->type.compare("BOOLEAN") == 0)
				value = new bool(*((bool *)(old->value)));
			else if(old->type.compare("FLOAT") == 0)
				value = new float(*((float *)(old->value)));
			else if(old->type.compare("DATE") == 0)
				value = new DATE(*((DATE *)(old->value)));

			column1 = new DATA(old->column1);
			column2 = new DATA(old->column2);
			next = NULL;
			prev = NULL;
			
		}
		
		void link(CONDLIST * second)
		{
			next = second;
			second->prev = this;
		}
		
	string column_name;
	string op;
	void * value; //commonly is of type DATA
	string type;
	CONDLIST * next;
	CONDLIST * prev;
	DATA * column1;
	DATA * column2;
};

class SELECTQUERYPARSE
{
	public:
		DATA * table;
		DATA * columnlist;
		DATA * display_constraint;
		CONDLIST * search_condition;
		DATA * group_grouping;
		DATA * group_condition;
		DATA * order_grouping;
};

/*
	possible inserttype values =  MANUAL | SUBQUERY
	if(inserttype == MANUAL)	void * = TABLEDATALIST
	else if(inserttype == SUBQUERY)	void * = SELECTQUERYPARSE
	
*/
class INSERTQUERYPARSE
{
	public:
		string tablename;
		DATA * datalist;
		string inserttype;
		
};

class UPDATEQUERYPARSE
{
	public:
		string tablename;
		DATA * column_value;
		CONDLIST * search_condition;
};

class DELETEQUERYPARSE
{
	public:
		string tablename;
		CONDLIST * search_condition;
};


class TABLE
{
	public:
		string tablename;
		TABLEDATALIST ** table;
		string alias;
};

typedef struct record {
	int value;
	TABLEDATALIST * data;
} record;

