TABLEDATALIST ** condlist_selector(	TABLEDATALIST ** table, 
									CONDLIST * search_condition, TABLEDATALIST * column_search = NULL, TABLEDATALIST * row_search = NULL
									)
{
	TABLEDATALIST 		** rows = NULL;
	
	CONDLIST 			* temp = search_condition;
	
	string 				opTemp,
						column_nameTemp,
						typeTemp,
						opTemp2 = "";
						
	if(temp == NULL)
	{
		rows = selector(table);
		return rows;
	}
	else
	{
		do
		{
			opTemp = temp->op;
			column_nameTemp = temp->column_name;
			typeTemp =  temp->type;
			
			if(opTemp.compare("<") == 0)
			{
				rows = selector(table, column_nameTemp, typeTemp, NULL, false, temp->value, false, rows, opTemp2, column_search, row_search);
			}
			else if(opTemp.compare(">") == 0)
			{
				rows = selector(table, column_nameTemp, typeTemp, temp->value, false, NULL, false, rows, opTemp2, column_search, row_search);
			}
			else if(opTemp.compare("<=") == 0)
			{
				rows = selector(table, column_nameTemp, typeTemp, NULL, false, temp->value, true, rows, opTemp2, column_search, row_search);
			}
			else if(opTemp.compare(">=") == 0)
			{
				rows = selector(table, column_nameTemp, typeTemp, temp->value, true, NULL, false, rows, opTemp2, column_search, row_search);
			}
			else if(opTemp.compare("=") == 0 && typeTemp.compare("INT") == 0) 
			{
				rows = selector(table, column_nameTemp, typeTemp, temp->value, true, temp->value, true, rows, opTemp2, column_search, row_search);
			}
			else if(opTemp.compare("=") == 0 && typeTemp.compare("VARCHAR") == 0) 
			{
				rows = selector(table, column_nameTemp, "=", static_cast<string *>(temp->value), rows, opTemp2, column_search, row_search);
			}
			else if (opTemp.compare("LIKE") == 0)
			{ 
				rows = selector(table, column_nameTemp, "LIKE", static_cast<string *>(temp->value), rows, opTemp2, column_search, row_search);
			}
			else if(opTemp.compare("ISNULL") == 0)
			{
				rows = selector(table, column_nameTemp, "ISNULL", rows, opTemp2, column_search, row_search);
			}
			else if (opTemp.compare("AND") == 0 || opTemp.compare("OR") == 0)
			{
				opTemp2 = opTemp;
			}
			else
			{
				opTemp2 = "";
			}
			temp = temp->next;
		}while(temp != NULL);
	}
	return rows;
}

int cond_stat(TABLEDATALIST ** table, CONDLIST * search_condition)
{
	
	CONDLIST 			* temp = search_condition;
	
	string 				opTemp,
						column_nameTemp,
						typeTemp,
						opTemp2 = "";
						
	int 				count;
						
	
	if(table == NULL)
		return -1;
	else
	{
	
		
		opTemp = temp->op;
		column_nameTemp = temp->column_name;
		typeTemp =  temp->type;
		
		if(opTemp.compare("<") == 0)
		{
			return row_count(table, column_nameTemp, typeTemp, NULL, false, temp->value, false);
		}
		else if(opTemp.compare(">") == 0)
		{
			return row_count(table, column_nameTemp, typeTemp, temp->value, false, NULL, false);
		}
		else if(opTemp.compare("<=") == 0)
		{
			return count = row_count(table, column_nameTemp, typeTemp, NULL, false, temp->value, true);
		}
		else if(opTemp.compare(">=") == 0)
		{
			return count = row_count(table, column_nameTemp, typeTemp, temp->value, true, NULL, false);
		}
		else if(opTemp.compare("=") == 0 && typeTemp.compare("INT") == 0) 
		{
			return count = row_count(table, column_nameTemp, typeTemp, temp->value, true, temp->value, true);
		}
		else if(opTemp.compare("=") == 0 && typeTemp.compare("VARCHAR") == 0) 
		{
			return count = row_count(table, column_nameTemp, "=", static_cast<string *>(temp->value));
		}
		else if (opTemp.compare("LIKE") == 0)
		{ 
			return count = row_count(table, column_nameTemp, "LIKE", static_cast<string *>(temp->value));
		}
	}
}