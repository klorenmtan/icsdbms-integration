
//list of structures 
#include<stdio.h>
#include<string.h>
#include<malloc.h>
#include<stdlib.h>


//structures
#include"structures/queryparse.h"
#include"structures/struct.h"

//functions
#include"functions/convert_query_parse_tree_to_relational_algebra.h"
#include"functions.h"

//testing
#include"testing/testing.h"

main()
{

	convert_query_parse_tree_to_relational_algebra(build_test_query());

}


