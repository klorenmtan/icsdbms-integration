TABLEDATALIST ** join_op(TABLE table, TABLE table2, CONDLIST * cond = NULL, string table_op = "")
{
	
	TABLEDATALIST 	* table_column,
					* table2_column,
					** table_data,
					** table2_data,
					* table_start,
					* table2_start,
					* table_end,
					* table2_end,
					** join_table;
	
	DATA			* temp,
					* column,
					* column2;
					
	
	string 			table_alias = table.alias,
					table2_alias = table2.alias;
	
	
	join_table = join(table.table, table2.table);
	
	if(cond != NULL)
	{
		
		column = cond->column1;
		column2 = cond->column2;
	}
	
	//mark the first columns of	the two tables
	table_start = join_table[0];
	table2_start = get_other_table(join_table);
	
	//mark the end columns of the two tables
	table2_end = table_start->prev;
	table_end = table2_start->prev;
	print_tabledatalist(join_table);
	if(cond == NULL)
		return join_table;
	else
	{
		
		temp = column;
		table_data = &join_table[0];
		table2_data = &table2_start;
		if(temp->up == NULL)
		{
			if(find_column(table_data, get_string(temp->data), table_end) != NULL && find_column(table2_data, get_string(temp->data), table2_end) != NULL)
			{
				//error: error("Column '" + get_string(temp->data)+ "'in 'where clause' is ambiguous");
				return NULL;
			}
			else if(find_column(table_data, get_string(temp->data), table_end) != NULL)
				table_column = find_column(table_data, get_string(temp->data));
			else if(find_column(table2_data, get_string(temp->data)) != NULL, table2_end)
				table_column = find_column(table_data, get_string(temp->data), table_end);
			else
			{
				//error: error("Unknown column '" + get_string(temp->data)+ "'in 'where clause'");
				return NULL;
			}
		}
		else
		{	
			if(get_string(temp->up->data).compare(table_alias) == 0)
				table_column = find_column(table_data, get_string(temp->data), table_end);
			else if(get_string(temp->up->data).compare(table2_alias) == 0)
				table_column = find_column(table2_data, get_string(temp->data), table2_end);
			else
			{
				//error: error("Unknown column '" + get_string(temp->up->data) + "." +get_string(temp->data)+ "'in 'where clause'");
				return NULL;
			}
		}
		
		temp = column2;
		if(temp->up == NULL)
		{
			if(find_column(table_data, get_string(temp->data), table_end) != NULL && find_column(table2_data, get_string(temp->data), table2_end) != NULL)
			{
				//error: error("Column '" + get_string(temp->data)+ "'in 'where clause' is ambiguous");
				return NULL;
			}
			else if(find_column(table_data, get_string(temp->data), table_end) != NULL)
				table2_column = find_column(table_data, get_string(temp->data), table_end);
			else if(find_column(table2_data, get_string(temp->data),table2_end) != NULL)
				table2_column = find_column(table2_data, get_string(temp->data),table2_end);
			else
			{
				//error: error("Unknown column '" + get_string(temp->data)+ "'in 'where clause'");
				return NULL;
			}
		}
		else
		{
			if(get_string(temp->up->data).compare(table_alias) == 0)
				table2_column = find_column(table_data, get_string(temp->data), table_end);
			else if(get_string(temp->up->data).compare(table2_alias) == 0)
				table2_column = find_column(table2_data, get_string(temp->data), table2_end);
			else
			{
				//error: error("Unknown column '" + get_string(temp->up->data) + "." +get_string(temp->data)+ "'in 'where clause'");
				return NULL;
			}
		}
		
		if(table_op.compare("INNER_JOIN") == 0)
		{
			//free_tabledatalist(join_table);
			return inner_join(join_table, table_column, table2_column, cond->op);
			//free_tabledatalist(join_table);
		}
		else if(table_op.compare("LEFT_JOIN") == 0)
		{
			return left_join(join_table, table_column, table2_column, cond->op, row_count(table2.table[0]), table2_data[0]);
		}
		else if(table_op.compare("RIGHT_JOIN") == 0)
		{
			return right_join(join_table, table_column, table2_column, cond->op, row_count(table.table[0]), row_count(table2.table[0]), table2_data[0]);
		}
		else if(table_op.compare("FULL_JOIN") == 0)
		{
			return full_join(join_table, table_column, table2_column, cond->op, row_count(table.table[0]), row_count(table2.table[0]), table2_data[0]);
		}
	}
}

TABLEDATALIST ** right_join(TABLEDATALIST ** join_table, TABLEDATALIST * column, TABLEDATALIST * column2, string cond_op, int row_count_table1, int row_count_table2, 
					TABLEDATALIST * table2_start)
{
	TABLEDATALIST 	* temp = join_table[0]->down->down,
					* temp2 = column->down->down,
					* temp3 = column2->down->down,
					* temp4 = table2_start->down->down,
					** rows,
					** selected_rows;
	
	CONDLIST 		* cond_temp = new CONDLIST();
	
	
	int 			count = 0,
					i = 0,
					r_count = row_count(join_table[0]);
					
	bool			has_selected[row_count_table2];
	
	for(int j = 0; j < row_count_table2; j++)
		has_selected[j] = false;
	
	selected_rows = (TABLEDATALIST **)malloc(sizeof(TABLEDATALIST *) * r_count + 1);
	
	for(int i = 0; i < r_count + 1; i++)
		*(selected_rows + i) = NULL;
	
	cond_temp->op = cond_op;				
		
	do
	{
		cond_temp->value = temp3->data;
		cond_temp->type = temp3->type;
		rows = condlist_selector(join_table, cond_temp, column, temp2);
		
		i++;
		
		if(*(rows + 0) != NULL)
		{
			*(selected_rows + count++) = *(rows + 0);
			*(rows + 0) = NULL;
			has_selected[i % row_count_table2] = true;
		}
		
		if(i > ((row_count_table1 - 1) * row_count_table2))
		{
			if(has_selected[row_count_table2 - (row_count_table1 * row_count_table2 - i)] == false)
			{
				*(selected_rows + count++) = temp;
				make_row_data_null(temp, temp4);
			}
		}	
		
		temp4 = temp4->down;
		temp3 = temp3->down;
		temp2 = temp2->down;
		temp = temp->down;
		
	}
	while(temp != join_table[0]);
		
	return create_new_tabledatalist(join_table, selected_rows);

}


TABLEDATALIST ** full_join(TABLEDATALIST ** join_table, TABLEDATALIST * column, TABLEDATALIST * column2, string cond_op, int row_count_table1, int row_count_table2, 
							TABLEDATALIST * table2_start)
{	
	TABLEDATALIST 	* temp = join_table[0]->down->down,
					* temp2 = column->down->down,
					* temp3 = column2->down->down,
					* temp4 = table2_start->down->down,
					** rows,
					** selected_rows;
	
	CONDLIST 		* cond_temp = new CONDLIST();
	
	
	int 			count = 0,
					i = 0,
					r_count = row_count(join_table[0]);
					
	bool			has_selected[row_count_table2],
					selected = false;
	
	for(int j = 0; j < row_count_table2; j++)
		has_selected[j] = false;
	
	selected_rows = (TABLEDATALIST **)malloc(sizeof(TABLEDATALIST *) * r_count + 1);
	
	for(int i = 0; i < r_count + 1; i++)
		*(selected_rows + i) = NULL;
	
	cond_temp->op = cond_op;				
		
	do
	{
		cond_temp->value = temp3->data;
		cond_temp->type = temp3->type;
		rows = condlist_selector(join_table, cond_temp, column, temp2);
		
		i++;
		
		if(*(rows + 0) != NULL)
		{
			*(selected_rows + count++) = *(rows + 0);
			*(rows + 0) = NULL;
			selected = true;
			has_selected[i % row_count_table2] = true;
		}
		
		if(i % row_count_table2 == 0 )
		{
			if(selected == false)
			{
				*(selected_rows + count++) = temp;
				make_row_data_null(temp4, temp);
			}
			selected = false;
		}	
		if(i > ((row_count_table1 - 1) * row_count_table2))
		{
			if(has_selected[row_count_table2 - (row_count_table1 * row_count_table2 - i)] == false)
			{
				*(selected_rows + count++) = temp;
				make_row_data_null(temp, temp4);
			}
		}	
		
		temp4 = temp4->down;
		temp3 = temp3->down;
		temp2 = temp2->down;
		temp = temp->down;
		
	}
	while(temp != join_table[0]);
		
	return create_new_tabledatalist(join_table, selected_rows);
}



TABLEDATALIST ** left_join(TABLEDATALIST ** join_table, TABLEDATALIST * column, TABLEDATALIST * column2, string cond_op, int row_count_table2, 
							TABLEDATALIST * table2_start)
{	
	TABLEDATALIST 	* temp = join_table[0]->down->down,
					* temp2 = column->down->down,
					* temp3 = column2->down->down,
					* temp4 = table2_start->down->down,
					** rows,
					** selected_rows;
	
	CONDLIST 		* cond_temp = new CONDLIST();
	
	
	int 			count = 0,
					i = 0,
					r_count = row_count(join_table[0]);
					
	bool			has_selected = false;
	
	selected_rows = (TABLEDATALIST **)malloc(sizeof(TABLEDATALIST *) * r_count + 1);
	
	for(int i = 0; i < r_count + 1; i++)
		*(selected_rows + i) = NULL;
	
	cond_temp->op = cond_op;				
		
	do
	{
		cond_temp->value = temp3->data;
		cond_temp->type = temp3->type;
		rows = condlist_selector(join_table, cond_temp, column, temp2);
		if(*(rows + 0) != NULL)
		{
			*(selected_rows + count++) = *(rows + 0);
			*(rows + 0) = NULL;
			has_selected = true;
		}
		
		i++;
		
		if(i % row_count_table2 == 0 )
		{
			if(has_selected == false)
			{
				*(selected_rows + count++) = temp;
				make_row_data_null(temp4, temp);
			}
			has_selected = false;
		}	
		
		temp4 = temp4->down;
		temp3 = temp3->down;
		temp2 = temp2->down;
		temp = temp->down;
		
	}
	while(temp != join_table[0]);
		
	return create_new_tabledatalist(join_table, selected_rows);
}


TABLEDATALIST ** inner_join(TABLEDATALIST ** join_table, TABLEDATALIST * column, TABLEDATALIST * column2, string cond_op)
{	
	TABLEDATALIST 	* temp = join_table[0]->down->down,
					* temp2 = column->down->down,
					* temp3 = column2->down->down,
					** rows,
					** selected_rows;
	
	CONDLIST 		* cond_temp = new CONDLIST();
	
	
	int 			count = 0,
					r_count = row_count(join_table[0]);
	
	selected_rows = (TABLEDATALIST **)malloc(sizeof(TABLEDATALIST *) * r_count + 1);
	
	for(int i = 0; i < r_count + 1; i++)
		*(selected_rows + i) = NULL;
	
	cond_temp->op = cond_op;				
		
	
		do
		{
			cond_temp->value = temp3->data;
			cond_temp->type = temp3->type;
			rows = condlist_selector(join_table, cond_temp, column, temp2);
			if(*(rows + 0) != NULL)
			{
				*(selected_rows + count++) = *(rows + 0);
				*(rows + 0) = NULL;
			}
			temp3 = temp3->down;
			temp2 = temp2->down;
			temp = temp->down;
		}
		while(temp != join_table[0]);
		
	return create_new_tabledatalist(join_table, selected_rows);
}

TABLEDATALIST ** join(TABLEDATALIST ** table, TABLEDATALIST ** table2)
{
	TABLEDATALIST 	** join = get_table_header(table),
					** temp_table2_header = get_table_header(table2),
					* temp = join[0]->down,
					* temp2,
					* temp3,
					* temp_table = table[0]->down->down,
					* temp_table2 = table2[0]->down->down;
	
	connect_two_rows_horizontal(join[0], temp_table2_header[0]);	
	connect_two_rows_horizontal(join[1], temp_table2_header[1]);
	
	temp = join[1];
	do
	{	
		do
		{
			temp2 = copy_row(temp_table);
			temp3 = copy_row(temp_table2);
			connect_two_rows_horizontal(temp2, temp3);
			connect_two_rows_vertical(temp, temp2);
			temp_table2 = temp_table2->down;	
			temp = temp->down;
			//free(temp->up);
		}
		while(temp_table2 != table2[0]);	
		temp_table  = temp_table->down;
		temp_table2  = table2[0]->down->down;
		
	}
	while(temp_table != table[0]);
	
	
	connect_two_rows_vertical(temp, join[0]);
	//circular_test(join);
	//print_tabledatalist(join);
	temp_table2_header[0]->prev->next = NULL;
	temp_table2_header[0]->prev = NULL;
	
	return join;
}

TABLEDATALIST * get_other_table(TABLEDATALIST ** table)
{
	TABLEDATALIST * temp = table[0];
	
	while(temp->next != NULL)
		temp = temp->next;
	
	
	temp->next = temp->down->next->up;
	temp = temp->next;
	temp->prev = temp->down->prev->up;
	
	return temp;	
}