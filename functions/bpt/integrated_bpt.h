TABLEDATALIST ** order_by(TABLE table, DATA * order_grouping)
{
	TABLEDATALIST 	** tabledatalist = table.table,
					* temp = tabledatalist[0],
					** rows = NULL;
	
	node			* root = NULL;
					
	do
	{
		if(get_string(temp->data).compare(get_string( order_grouping->data)) == 0)
			break;
		temp = temp->next;
	}
	while(temp != tabledatalist[0]);
	
	if(temp != tabledatalist[0])
	{
		if(order_grouping->down == NULL || get_string(order_grouping->down->data).compare("ASC") == 0)
		{
			root = bpt_index_exists(table.tablename, get_string(order_grouping->data));
			if(root != NULL)
			{
				rows = sort_asc(root, table.table);
			}
			else
			{
				rows = sort_asc(insert_to_bpt_pool(table.tablename,
				get_string(order_grouping->data),
				build_bpt_index(table.table, get_column(table.table[0], get_string(order_grouping->data)))),
				table.table);
			}
		}
		else if(get_string(order_grouping->down->data).compare("DESC") == 0)
		{
			root = bpt_index_exists(table.tablename, get_string(order_grouping->data));
			
			if(root != NULL)
			{
				rows = sort_desc(root, table.table);
			}
			else
			{
				rows = sort_desc(insert_to_bpt_pool(table.tablename,
				get_string(order_grouping->data),
				build_bpt_index(table.table, get_column(table.table[0], get_string(order_grouping->data)))),
				table.table);
			}
			
		}
	}
}

node * build_bpt_index( TABLEDATALIST ** table, TABLEDATALIST * column )
{
	TABLEDATALIST 	* temp = table[0]->down->down,
					* temp2 = column->down->down;
	node 			* root = NULL;
	
	if( get_string ( column->down->data ).compare( "INT" ) != 0 )
		return NULL;
		
	do
	{
		root = insert( root, get_int ( temp2->data ), get_int( temp2->data ), temp );
		temp = temp->down;
		temp2 = temp2->down;
	}
	while( temp != table[0] );
	
	return root;
}

TABLEDATALIST ** sort_asc(node * root,  TABLEDATALIST ** table)
{
	TABLEDATALIST ** rows;
	rows = get_leaves_asc(root, row_count(table[0]));
	if(rows == NULL)
		cout << "NULL";
	
	/*
	for(int count = 0; *(rows + count) != NULL; count++)
	{
		cout << get_string((*(rows + count))->data) << "\n";
	}
	*/
	
	return rows;
}

TABLEDATALIST ** sort_desc(node * root,  TABLEDATALIST ** table)
{
	TABLEDATALIST ** rows;
	rows = get_leaves_desc(root, row_count(table[0]));
	if(rows == NULL)
		cout << "NULL";
		
	/*	
	for(int count = 0; *(rows + count) != NULL; count++)
	{
		cout << get_string((*(rows + count))->data) << "\n";
		
	}
	*/
	
	return rows;
}

node * bpt_index_exists(string tablename, string columnname)
{
	
	for(int i = 0; *(bpt_pool + i) != NULL; i++)
	{
		if((*(bpt_pool + i))->table_name.compare(tablename) == 0 && (*(bpt_pool + i))->column_name.compare(columnname) == 0)
			return (*(bpt_pool + i))->root;
	}
	return NULL;
}

node * insert_to_bpt_pool(string table_name, string column_name, node * root)
{
	int 	i;
	
	for(i = 0; *(bpt_pool + i) != NULL; i++);
	
	*(bpt_pool + i) = new BPT(table_name, column_name, root);
	
	return root;
}
