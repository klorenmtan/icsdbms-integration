TABLEDATALIST * find_row_head(TABLEDATALIST ** table, TABLEDATALIST * cell)
{
	TABLEDATALIST * temp1 = table[0], * temp2 = cell;
	//cout << "Table head" << *(static_cast<string *>(table[0]->data));
	do
		{
		do
		{
			if(temp1 == temp2)
				return temp1;
				
			temp2 = temp2->next;
			
		}while(temp2 != cell);
		
		temp1 = temp1->down;
		//cout << "Row";
	}while(temp1 != table[0]);
	return NULL;
}

TABLEDATALIST * find_first_null_row(TABLEDATALIST * column)
{
	TABLEDATALIST *	temp = column;
	
	if(column == NULL) return NULL;
	
	while(temp->down != NULL)
	{
		temp = temp->down;
	}
	
	return temp;
}

bool create_new_cell(void * data, string type, TABLEDATALIST * next, TABLEDATALIST * prev, TABLEDATALIST * up, TABLEDATALIST * down)
{
	TABLEDATALIST * temp = new TABLEDATALIST(data, type, "", next, prev, up, down);
	
	if(temp->up != NULL)
		temp->up->down = temp;
	if(temp->prev != NULL)
		temp->prev->next = temp;
	if(temp->next != NULL)
		temp->next->prev = temp;
	
	return true;
}

bool remove_row(TABLEDATALIST * row)
{
	TABLEDATALIST * temp = row;
	
	temp->prev->next = NULL;
	
	
	do
	{
		temp->up->down = temp->down;
		temp->down->up = temp->up;
		temp = temp->next;
		free(temp->prev);
	}
	while(temp->next != NULL);
	
	temp->up->down = temp->down;
	temp->down->up = temp->up;
	free(temp);
	
	return true;
	
}

bool append_new_column(TABLEDATALIST ** table, string column_name, string type)
{
	TABLEDATALIST * temp = table[0]->prev;
	
	temp->next = new TABLEDATALIST(new string(column_name), "VARCHAR");
	temp->next->next = table[0];
	temp->next->prev = temp; 
	table[0]->prev = temp->next;
	
	temp = temp->next;
	temp->down = new TABLEDATALIST(new string(type), "VARCHAR");
	temp->down->up = temp;
	temp->down->prev = temp->prev->down;
	temp->down->next = table[1];
	table[1]->prev = temp;
	
	return true;
	
}

TABLEDATALIST * find_last_equal_row(TABLEDATALIST ** table, TABLEDATALIST * start_row, TABLEDATALIST * first_row)
{
	TABLEDATALIST * temp = start_row;
	do
	{
		if(is_equal(start_row, temp))
			temp = temp->down;
		else
			return temp->up;
	}
	while(temp != first_row);
	
	return temp->up;
}

bool swap_rows(TABLEDATALIST * row1, TABLEDATALIST * row2)
{
	TABLEDATALIST 	* temp = row1, 
					* temp2 = row2,
					* temp3,
					* temp4,
					* up,
					* down,
					* next,
					* prev;
					
	do
	{
			temp3 = temp->next;	
			temp4 = temp2->next;
		if(row1 == row2)
			return true;
		else if(temp->down != temp2)
		{
		
			up = temp->up;
			down = temp->down;

			
			up->down = temp2;
			down->up = temp2;
			
			temp->up = temp2->up;
			temp->down = temp2->down;
			
			
			temp2->up->down = temp;
			temp2->down->up = temp;
			
			temp2->up = up;
			temp2->down = down;
			
		}
		else
		{
			temp->up->down = temp2;
			temp2->down->up = temp;
			temp2->up = temp->up;
			temp->down = temp2->down;
			temp2->down = temp;
			temp->up = temp2;
			
		}
			temp = temp3;	
			temp2 = temp4;
	}
	while(temp != row1);
}

TABLEDATALIST * make_row_data_null(TABLEDATALIST * row, TABLEDATALIST * end = NULL)
{
	TABLEDATALIST * temp = row;
	
	if(end == NULL)	
	{
		do
		{
			temp->type = "";
			free(temp->data);
			temp->data = NULL;
			temp = temp->next;
		}while(temp != row);
	}
	else
	{
		do
		{
			temp->type = "";
			free(temp->data);
			temp->data = NULL;
			temp = temp->next;
		}
		while(temp != end);		
	}
	return row;
}

void connect_two_rows_horizontal(TABLEDATALIST * row, TABLEDATALIST * row2)
{
	TABLEDATALIST 	* temp,
					* temp2;
	
	temp = find_row_end(row);
	temp->next = row2;
	temp2 = find_row_end(row2);
	temp2->next = row;
	row2->prev = temp;
	row->prev = temp2;
}

void connect_two_rows_vertical(TABLEDATALIST * row, TABLEDATALIST * row2)
{
	TABLEDATALIST * temp = row,
				  * temp2 = row2;
	do
	{
		temp->down = temp2;
		temp2->up = temp;
		temp = temp->next;
		temp2 = temp2->next;
	}
	while(temp != row && temp2 != row2 && temp != NULL && temp2 != NULL);
	
}

TABLEDATALIST * find_row_end(TABLEDATALIST * row)
{
	return row->prev;
}

TABLEDATALIST * copy_row(TABLEDATALIST * row)
{
	TABLEDATALIST 	* temp = row,
					* tail = NULL,
					* head = NULL;
	do
	{
		if(head == NULL)
		{
			head = new TABLEDATALIST(temp);
			tail = head;
		}
		else
		{
			tail->next = new TABLEDATALIST(temp);
			tail->next->prev = tail;
			tail = tail->next;
		}
		temp = temp->next;
	}
	while(temp != row);
	
	tail->next = head;
	head->prev = tail;
	
	return head;
}

TABLEDATALIST * find_column(TABLEDATALIST ** table, string column, TABLEDATALIST * end = NULL)
{
	TABLEDATALIST * temp = table[0];
	
	if(end == NULL)	
	{
		do
		{
			if(column.compare(get_string(temp->data)) == 0)
				return temp;
			temp = temp->next;
		}while(temp != table[0]);
	}
	else
	{
		do
		{
			if(column.compare(get_string(temp->data)) == 0)
				return temp;
			temp = temp->next;
		}while(temp != end);		
	}
	
	return NULL;
}

bool remove_column(TABLEDATALIST ** table, DATA * columnlist)
{
	TABLEDATALIST 		* temp = table[0],
						* temp1,
						* temp2;
	
	DATA 				* dataTemp = columnlist;
	
	string 				stringTemp;
	do
	{
		do{
			stringTemp = get_string(dataTemp->data);
			if(get_string(temp->data).compare(stringTemp) == 0)
			{
				temp1 = temp;
				temp1->up->down = NULL;
				do
				{
					if(temp1 == table[0])
						table[0] = temp1->next;
					if(temp1 == table[1])
						table[1] = temp1->next;
					if(temp1->next == temp1)
					{
						table[0] = NULL;
					}
					temp1->prev->next = temp1->next;
					temp1->next->prev = temp1->prev;
					temp2 = temp1;
					temp1 = temp1->down;
					free(temp2);
				}while(temp1 != NULL);
				break;
			}
			temp = temp->next;
		}while(temp != table[0]);
		temp = table[0];
		dataTemp = dataTemp->next;
	}while(dataTemp != NULL);
}


 TABLEDATALIST ** get_table_header(TABLEDATALIST ** table)
 {

	if(table == NULL)
		return NULL;
	
	TABLEDATALIST 	* temp1 = table[0], 
					* temp2 = NULL, 
					** newtable, 
					* temp4 = NULL, 
					* temp5 = NULL, 
					* temp6, 
					* previousRow = NULL,
					* previousColumn = NULL,
					* oldrow = NULL,
					* temp7 = NULL,
					* temp8 = NULL;
	DATA 			* dataTemp;
	bool			boolTemp;
	
	newtable = (TABLEDATALIST **)malloc(sizeof(TABLEDATALIST *)*2);
	newtable[0] = new TABLEDATALIST(table[0]);
	newtable[1] = new TABLEDATALIST(table[1]);
	newtable[0]->down = newtable[1];
	newtable[1]->up = newtable[0];
	
	
	//copying the first two rows : the column name and column data type
	for(int i=0; i < 2; i++)
	{
		temp6 = temp4 = temp5 = newtable[i];
		temp2 = temp1->next;
		do
		{
			temp4 = new TABLEDATALIST(temp2);
			temp4->prev = temp5;
			temp5->next = temp4;
			temp5 = temp4;
			temp2 = temp2->next;
			if(temp2 == temp1)
			{
				temp4->next = temp6;
				temp6->prev = temp4;
			}
		}while(temp2 != temp1);

		temp1 = table[i+1];
	}
	
	temp1 = temp2 = newtable[0];
	temp4 = newtable[1];
	
	do
	{
		temp1->up = NULL;
		temp1->down = temp4;
		temp4->up = temp1;
		temp4->down = NULL;
		temp1 = temp1->next;
		temp4 = temp4->next;
	}while(temp1 != temp2);
	
	return newtable;
	
	//end of copying of the first two rows: the column name and column data type
 }
 
 TABLEDATALIST ** create_new_tabledatalist(TABLEDATALIST ** table, TABLEDATALIST ** rows, DATA * columnlist = NULL)
 {
	if(table == NULL || rows == NULL)
		return NULL;
	
	TABLEDATALIST 	* temp1 = table[0], 
					* temp2 = NULL, 
					** temp3 = rows, 
					** newtable, 
					* temp4 = NULL, 
					* temp5 = NULL, 
					* temp6, 
					* previousRow = NULL,
					* previousColumn = NULL,
					* oldrow = NULL,
					* temp7 = NULL,
					* temp8 = NULL;
	DATA 			* dataTemp;
	bool			boolTemp;
	
	newtable = (TABLEDATALIST **)malloc(sizeof(TABLEDATALIST *)*2);
	newtable[0] = new TABLEDATALIST(table[0]);
	newtable[1] = new TABLEDATALIST(table[1]);
	newtable[0]->down = newtable[1];
	newtable[1]->up = newtable[0];
	
	
	//copying the first two rows : the column name and column data type
	for(int i=0; i < 2; i++)
	{
		temp6 = temp4 = temp5 = newtable[i];
		temp2 = temp1->next;
		do
		{
			temp4 = new TABLEDATALIST(temp2);
			temp4->prev = temp5;
			temp5->next = temp4;
			temp5 = temp4;
			temp2 = temp2->next;
			if(temp2 == temp1)
			{
				temp4->next = temp6;
				temp6->prev = temp4;
			}
		}while(temp2 != temp1);

		temp1 = table[i+1];
	}
	
	temp1 = temp2 = newtable[0];
	temp4 = newtable[1];
	
	do
	{
		temp1->down = temp4;
		temp4->up = temp1;
		temp1 = temp1->next;
		temp4 = temp4->next;
	}while(temp1 != temp2);
	
	
	//end of copying of the first two rows: the column name and column data type
	
	//temp4, temp5, temp6 are available temporary variables
	previousRow = newtable[0];
	
	//copying of the rows listed
	for(int i=0; *(rows + i)!=NULL; i++)
	{
		
		previousRow = previousRow->down;
		previousRow->down = new TABLEDATALIST(*(rows + i ));
		temp4 = previousRow->down;
		temp6 = temp5 = (*(rows + i))->next;
		
		do{
			temp4->next = new TABLEDATALIST(temp5);
			temp4->next->prev = temp4;
			temp5 = temp5->next;
			temp4 = temp4->next;
		}while(temp6 != temp5->next);
			
		temp4->next = previousRow->down;
		previousRow->down->prev = temp4;
		
		temp7 = previousRow;
		//temp6 = temp6->prev;
		temp6 = previousRow->down;
		
		do
		{
			temp7->down = temp6;
			temp6->up = temp7;
			temp7 = temp7->next;
			temp6 = temp6->next;
		}while(temp7 != previousRow);
		
		//circular_test(table);
	
	}
	
	
	temp5 = previousRow->down;
	temp6 = temp4 = newtable[0];	
	do
	{
		temp4->up = temp5;
		temp5->down = temp4;
		temp4 = temp4->next;
		temp5 = temp5->next;
	}while(temp4 != temp6);
	
	//end of copying of rows listed
	
	
	temp6 = temp5 = temp7 = newtable[0];
	
	// circular_test(newtable); : PASS : CORRECT DATA
	
	if(columnlist == NULL || (*(static_cast<string *>(columnlist->data))).compare("ALL") == 0)
	{	
		return newtable;
	}
	else
	{
		
		for(previousColumn = NULL, temp8 = newtable[0]; columnlist != NULL; columnlist = columnlist->next)
		{			
			temp7 = temp8;
			
			while(temp7 != NULL)
			{
				
				if((*(static_cast<string *>(columnlist->data))).compare((*(static_cast<string *>(temp7->data)))) == 0)
				{
					boolTemp = true;
					break;
				}
				temp7 = temp7->next;
			}
			
			temp5 = temp7;
			
			if(previousColumn == NULL)
			{
				do
				{
					temp5->prev = NULL;
					temp5 = temp5->down;
				}while(temp5 != temp7);
				newtable[0] = temp7;
				newtable[1] = temp7->down;
			}
			else
			{
				temp6 = previousColumn;
				do
				{
					temp5->prev = temp6;
					temp6->next = temp5;
					temp5 = temp5->down;
					temp6 = temp6->down;
				}while(temp5 != temp7);
				
			}
			
			previousColumn = temp7;
	
			if(columnlist->next == NULL)
			{
				temp5 = previousColumn;
				temp7 = newtable[0];
				do
				{
					temp7->prev = temp5;
					temp5->next = temp7;
					temp7 = temp7->down;
					temp5 = temp5->down;				
				}while(temp5 != previousColumn);
				
			}			
				
		}
		
		//circular_test(newtable); : PASS : Correct data
	}
	
	return newtable;
 }
 
 TABLEDATALIST ** get_table(string tablename)
 {
	for(int i=0; i<10; i++)
	{
		if(table_pool[i]->tablename.compare("EMPLOYEES") == 0)
		{
			//return build_test_table();
			//for(int i=0; i<5; i++)
			
				//table = insert_op(build_sample_insert());
				//table = insert_physical_operator(table, build_sample_insert_data(),"MANUAL");
			
			return table_pool[i]->table;
		}
	}
	
	return NULL;	
 }
 
 TABLEDATALIST *  get_primary_key(TABLEDATALIST ** table)
 {
	TABLEDATALIST * temp = table[1];
	

	do
	{
		if(temp->constraint.compare("PRIMARY") == 0)
			return temp->up;
		temp = temp->next;
	}
	while(temp != table[1]);
	
	return NULL;
 }
 
 TABLEDATALIST * get_column(TABLEDATALIST * table, string column_name)
 {
	TABLEDATALIST * temp = table;
	
	do
	{
		if(get_string(temp).compare(column_name) == 0)
			return temp;
		temp = temp->next;
	}
	while(temp != table);
	
	return NULL;
 
 }
 
 TABLE * get_table_from_table_pool(string table_name)
 {
	for(int i = 0; (*(table_pool + i)) != NULL; i++)
	{
		if((*(table_pool + i))->tablename.compare(table_name) == 0)
			return *(table_pool + i);
	}
 }
 
 TABLEDATALIST * get_cell(TABLEDATALIST ** table, TABLEDATALIST * row, string column_name, TABLEDATALIST * column_search = NULL)
 {
	TABLEDATALIST 	* temp = table[0],
					* temp2 = row;
	do
	{
		if(column_search == NULL && get_string(temp->data).compare(column_name) == 0)
			return temp2;
		else if(temp == column_search)
			return temp2;
			
		temp = temp->next;
		temp2 = temp2->next;
	}
	while( temp != table[0]);
	
	return NULL;
 }
 
 void free_tables()
 {
	for(int i = 0; (*(table_pool + i)) != NULL; i++)
	{
		free_tabledatalist((*(table_pool + i))->table);
		free(*(table_pool + i));
	}
	cout << "Tables freed\n";
 }
 
 bool free_tabledatalist(TABLEDATALIST ** tabledatalist)
{
	/*printing of the content of the tabledatalist*/
	TABLEDATALIST 		* temp1,
						* temp2;
						
	int 				count = 0;
	
	//cout << convert_to_string(tabledatalist[0]) << "string";
	if (tabledatalist == NULL || tabledatalist[0] == NULL)
	{
		cout << "TABLE EMPTY";
		return false;
	}
	temp2 = temp1 = tabledatalist[0];
	
	do{
		if(temp1->up != tabledatalist[0])
		{
			do
			{
				
				temp1 = temp1->next;	
				free(temp1->prev);
			}while(temp1!=temp2);
			cout << "\n";	
		}
		temp2 = temp2->down;
		free(temp2->up);
		temp1 = temp2;		
	//}while(temp2 != NULL);
	}while(temp2 != tabledatalist[0]);
	
	free(temp2);
	
	return true;
}