TABLEDATALIST ** group_by(TABLEDATALIST ** table, DATA * column)
{
	print_tabledatalist(table);
	TABLEDATALIST 	* col = find_column(table, get_string(column->data)),
					* temp,
					* temp2;
	if(col != NULL)
	{
		temp = col->down->down;
		while(temp != col)
		{
			temp2 = temp->down;
			do
			{
				if(is_equal(temp, temp2) && temp != temp2)
				{
					swap_rows(temp->down, temp2);
					temp = temp->down;
				}	
				temp2 = temp2->down;
			}
			while(temp2 != col);
			
			temp = temp->down;	
		}
	}
	else
		return NULL;
	
	return table;
}