#include"include.h"

void test_menu();
main()
{
	bpt_pool = (BPT **) malloc(sizeof(BPT *) * 100);
	table_pool = (TABLE **) malloc(sizeof(TABLE *) * 10);
		
	for(int i = 0; i < 100; i++)
		*(bpt_pool + i) = NULL;
	for(int i = 0; i < 10; i++)
		*(table_pool + i) = NULL;
	
	//convert_query_parse_tree_to_relational_algebra(build_test_query());	
	TABLEDATALIST ** table, **temp;
	int i = 0, choice;
	string strTemp;
	char charTemp[100], cur_input;
	
	table_pool[0] = build_test_table();
	//print_tabledatalist(table_pool[0]->table);
	
	table_pool[1] = build_test_table3();
	//print_tabledatalist(table_pool[1]->table);
	temp = group_by(table_pool[0]->table, new DATA("VARCHAR",new string("name")));
	//free_tabledatalist(table_pool[0]->table);
	table_pool[0]->table = temp;

	avg_aggregate_operator(temp, find_column(table_pool[0]->table, "age"), find_column(table_pool[0]->table, "gender"));
	
	//print_tabledatalist(table_pool[0]->table);
	//print_tabledatalist(temp);
	//sample_join5();
	
	/*
	*(table_pool + 0) = build_test_table();
	
	insert_op(build_sample_insert3());
	
	bpt_pool[0] = new BPT ("EMPLOYEES", "age", build_bpt_index(table_pool[0]->table, get_primary_key(table_pool[0]->table)));
	
	sort_asc(bpt_pool[0]->root, table_pool[0]->table);
	
	cout << get_int(column_max("EMPLOYEES", "age"));
	
	*/
	//free_tables();
	
	
	//circular_test(update_op(build_sample_update8()));
	//select_op(build_sample_select());
	//match_pattern(build_test_table()->table, sample_condlist2());
	
	test_menu();
	
	//insert_op(build_sample_insert());
	//print_tabledatalist(table_pool[0].table);
	//round(table_pool[0].table[0]->next);
	//print_tabledatalist(table_pool[0].table);
	//table_pool[1].table = create_new_tabledatalist(table_pool[0].table, condlist_selector(table_pool[0].table, sample_condlist()), build_sample_columnlist());
	//print_tabledatalist(table_pool[1].table);
	
	
	//remove_column(table_pool[0].table, build_sample_columnlist());
	//print_tabledatalist(table_pool[0].table);
	//print_column (length(table_pool[0].table[0]->next));
	//print_tabledatalist(table_pool[0].table);
	
	//print_tabledatalist(insert_op(build_sample_insert()));
	//print_tabledatalist(insert_op(build_sample_insert2()));
	
	
	//circular_test(table);
	//print_tabledatalist(update_op(build_sample_update()));
	/*for(int i=0;i<1000;i++)
	{
		table = insert_physical_operator(table, build_sample_insert_data(),"MANUAL");
	}
	*/
	
	//circular_test(table);
	
	//print_data(table[0]->down->down->down->down->down->prev->prev);
	//print_tabledatalist(delete_op(build_sample_delete()));
			//temp = create_new_table(table, get_all_rows(table), build_sample_columnlist());
	/*
	clock_t init, final;
	init=clock();
	for(int i = 0; i < 1000000; i++)
	{
		//cout << i << "\n";
		temp = insert_physical_operator(table, build_sample_insert_data(),"MANUAL");
		if(temp != NULL)
			table = temp;
	}*
	/
	//print_tabledatalist(table);
	
	
	final = clock()-init;
	cout << (double)final / ((double)CLOCKS_PER_SEC);
	
	//print_tabledatalist(table);
	//printf("Row count: %d\n",row_count(table, "age", "INT", new int(22), true, new int(22), true));
	//temp = selector(table, "age", "INT", new int(17), true, new int(22), true);
	
			/*
			do
			{
				cout << *(static_cast<string *>((*(temp+i))->data));
				i++;
				cout << "Loop";
			}while(*(temp+i) != NULL);
			*/
	

	//printf("Row count: %d\n", row_count(table, "name", new string("[^s]%")));
	
	//free(table);
	//free(temp);
}

void test_menu()
{
	TABLEDATALIST ** table, **temp;
	int i = 0, choice;
	string strTemp;
	char charTemp[100], cur_input;
	
	table_pool[0]= build_test_table();
	
	//printf("\nEnter table to be accessed:");
	
	//getline( cin, strTemp );
	//cout << strTemp;
	last_accessed = table_pool[0]->table;
	do
	{
		cout << "\nMenu\n";
		cout << "[1] Print current table\n"; 
		cout << "[2] INSERT INTO TABLE VALUES('tan', 22, 'F')\n"; 
		cout << "[3] INSERT INTO TABLE VALUES('SANTOS', NULL, 'F')\n"; 
		cout << "[4] INSERT INTO TABLE VALUES('aquino', NULL, NULL)\n"; 
		cout << "[5] UPDATE TABLE SET gender = S where age = 18\n";
		cout << "[6] UPDATE TABLE SET gender = S where name LIKE \"%s\"\n";
		cout << "[7] UPDATE TABLE SET gender = J where name LIKE \"%olis%\"\n";
		cout << "[8] UPDATE TABLE SET gender = J where name LIKE \"_q%\"\n";
		cout << "[9] UPDATE TABLE SET gender = X where name LIKE \"[a]%\"\n";
		cout << "[10] UPDATE TABLE SET gender = Z where name <= 17 AND name LIKE \"tan\" \n";
		cout << "[11] UPDATE TABLE SET gender = Z where name <= 17 OR age = 18 \n";
		cout << "[12] UPDATE TABLE SET gender = K where age = NULL \n";
		cout << "[13] SELECT age + 100 \"Added Age\" FROM TABLE\n";
		cout << "[14] DELETE FROM TABLE where name = 'solis ' OR gender LIKE \'%F%\'\n";
		cout << "[-1] Exit\n";
		cout << "Choice:";
		scanf("%d",&choice);
		switch(choice)
		{
			case 1: print_tabledatalist(last_accessed);
					write_tabledatalist(last_accessed,"test");
					break;
			case 2: insert_op(build_sample_insert3());
					print_tabledatalist(last_accessed);
					write_tabledatalist(last_accessed,"test");
					break;
			case 3: insert_op(build_sample_insert());
					print_tabledatalist(last_accessed);
					write_tabledatalist(last_accessed,"test");
					break;
			case 4: insert_op(build_sample_insert2());
					print_tabledatalist(last_accessed);
					write_tabledatalist(last_accessed,"test");
					break;
			case 5: update_op(build_sample_update());
					print_tabledatalist(last_accessed);
					write_tabledatalist(last_accessed,"test");
					break;
			case 6: update_op(build_sample_update2());
					print_tabledatalist(last_accessed);
					write_tabledatalist(last_accessed,"test");
					break;
			case 7: update_op(build_sample_update3());
					print_tabledatalist(last_accessed);
					write_tabledatalist(last_accessed,"test");
					break;
			case 8: update_op(build_sample_update4());
					print_tabledatalist(last_accessed);
					write_tabledatalist(last_accessed,"test");
					break;
			case 9: update_op(build_sample_update5());
					print_tabledatalist(last_accessed);
					write_tabledatalist(last_accessed,"test");
					break;
			case 10:update_op(build_sample_update6());
					print_tabledatalist(last_accessed);
					write_tabledatalist(last_accessed,"test");		
					break;
			case 11:update_op(build_sample_update7());
					//print_tabledatalist(last_accessed);
					write_tabledatalist(last_accessed,"test");
					break;
			case 12:update_op(build_sample_update8());
					//print_tabledatalist(last_accessed);
					write_tabledatalist(last_accessed,"test");
					break;
			case 13: select_op(build_sample_select());
					write_tabledatalist(last_accessed,"test");
					break;
			case 14:delete_op(build_sample_delete());
					//print_tabledatalist(last_accessed);
					write_tabledatalist(last_accessed,"test");
					break;
		}
	}while(choice != -1);
	
	free_tables();
}
